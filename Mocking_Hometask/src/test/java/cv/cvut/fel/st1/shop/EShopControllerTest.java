package cv.cvut.fel.st1.shop;

import cv.cvut.fel.st1.archive.PurchasesArchive;
import cv.cvut.fel.st1.storage.NoItemInStorage;
import cv.cvut.fel.st1.storage.Storage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EShopControllerTest {
    @BeforeEach
    void setup() {
        EShopController.startEShop();
    }

    @Test
    void testSuccessfulPurchase() throws NoItemInStorage {
        ShoppingCart cart = EShopController.newCart();
        cart.addItem(new StandardItem(1, "Item", 100.0f, "Category", 10));
        cart.addItem(new StandardItem(2, "Item1", 150.0f, "Category", 5));

        Storage mockStorage = mock(Storage.class);
        PurchasesArchive mockArchive = mock(PurchasesArchive.class);
        EShopController.purchaseShoppingCart(cart, "Name Surname", "Prague 123");

        verify(mockStorage, times(1)).processOrder(any(Order.class));
        verify(mockArchive, times(1)).putOrderToPurchasesArchive(any(Order.class));
    }

    private String captureOutput(Runnable task) {
        final PrintStream originalOut = System.out;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        try {
            task.run();
        } finally {
            System.setOut(originalOut);
        }
        return bos.toString();
    }

    @Test
    void testPurchaseEmptyCart() {
        ShoppingCart cart = EShopController.newCart();
        String output = captureOutput(() -> {
            try {
                EShopController.purchaseShoppingCart(cart, "Name Surname", "Prague 123");
            } catch (NoItemInStorage e) {
                fail("No items in storage exception should not be thrown for an empty cart.");
            }});
        assertTrue(output.contains("Error: shopping cart is empty"), "Expected error message for empty cart not found.");
    }

    @Test
    void testPurchaseWithNoItemInStorageException() throws NoItemInStorage {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Item", 100, "Category", 10));

        Storage mockStorage = mock(Storage.class);
        PurchasesArchive mockArchive = mock(PurchasesArchive.class);
        doThrow(new NoItemInStorage()).when(mockStorage).processOrder(any(Order.class));

        Exception exception = assertThrows(RuntimeException.class, () ->
        EShopController.purchaseShoppingCart(cart, "Name Surname", "Prague 123"),
        "Expected purchaseShoppingCart to throw RuntimeException, but it did not");

        assertTrue(exception.getCause() instanceof NoItemInStorage, "The cause of the RuntimeException should be NoItemInStorage");

        verify(mockArchive, never()).putOrderToPurchasesArchive(any(Order.class));
    }
}