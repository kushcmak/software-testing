package cv.cvut.fel.st1.archive;

import cv.cvut.fel.st1.shop.Item;
import cv.cvut.fel.st1.shop.Order;
import cv.cvut.fel.st1.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PurchasesArchiveTest {
    private PurchasesArchive archive;
    @BeforeEach
    void setUp() {
        archive = new PurchasesArchive();
    }

    @Test
    void putOrderToPurchasesArchive() {
        Order order = mock(Order.class);
        Item item = new StandardItem(1, "Item", 10.0f, "Category", 5);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        when(order.getItems()).thenReturn(items);
        archive.putOrderToPurchasesArchive(order);
        assertEquals(1, archive.getHowManyTimesHasBeenItemSold(item), "Item should be sold once after adding to archive.");
    }

    @Test
    void printItemPurchaseStatistics() {
        final PrintStream originalOut = System.out;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        try {
            archive.printItemPurchaseStatistics();
            assertTrue(bos.toString().contains("ITEM PURCHASE STATISTICS:"));
        } finally {
            System.setOut(originalOut);
        }
    }

    @Test
    void getHowManyTimesHasBeenItemSold() {
        Item item = new StandardItem(1, "Item", 10.0f, "Category", 5);
        int soldCount = 3;
        for (int i = 0; i < soldCount; i++) {
            Order order = mock(Order.class);
            ArrayList<Item> items = new ArrayList<>();
            items.add(item);
            when(order.getItems()).thenReturn(items);
            archive.putOrderToPurchasesArchive(order);
        }
        assertEquals(soldCount, archive.getHowManyTimesHasBeenItemSold(item), "The item sold count should match the expected value.");
    }

    @Test
    void ItemPurchaseArchiveEntryMocking() {
        Item item = new StandardItem(1, "Item", 10.0f, "Category", 5);
        ItemPurchaseArchiveEntry mockedEntry = mock(ItemPurchaseArchiveEntry.class);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive = new HashMap<>();
        itemPurchaseArchive.put(item.getID(), mockedEntry);

        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchive, new ArrayList<>());
        when(mockedEntry.getCountHowManyTimesHasBeenSold()).thenReturn(5);

        int soldCount = archive.getHowManyTimesHasBeenItemSold(item);
        assertEquals(5, soldCount);
        verify(mockedEntry).getCountHowManyTimesHasBeenSold();
    }
}