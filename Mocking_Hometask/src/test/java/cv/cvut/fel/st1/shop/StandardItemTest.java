package cv.cvut.fel.st1.shop;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {

    @Test
    void testConstructor() {
        int id = 1;
        String name = "Item";
        float price = 100.0f;
        String category = "Category";
        int loyaltyPoints = 10;
        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);
        assertAll("Constructor test",
                () -> assertEquals(id, item.getID()),
                () -> assertEquals(name, item.getName()),
                () -> assertEquals(price, item.getPrice()),
                () -> assertEquals(category, item.getCategory()),
                () -> assertEquals(loyaltyPoints, item.getLoyaltyPoints()));
    }

    @Test
    void testConstructorParametersAreNotNull() {
        int id = 1;
        String name = "Item";
        float price = 100.0f;
        String category = "Category";
        int loyaltyPoints = 10;
        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);
        assertAll("Constructor null parameters test",
                () -> assertNotNull(item.getName()),
                () -> assertNotNull(item.getCategory()));
    }

    @Test
    void testCopyMethod() {
        StandardItem original = new StandardItem(1, "Item", 100.0f, "Category", 10);
        StandardItem copy = original.copy();
        assertAll("Copy should have the same values but be a different item",
                () -> assertEquals(original.getID(), copy.getID()),
                () -> assertEquals(original.getName(), copy.getName()),
                () -> assertEquals(original.getPrice(), copy.getPrice()),
                () -> assertEquals(original.getCategory(), copy.getCategory()),
                () -> assertEquals(original.getLoyaltyPoints(), copy.getLoyaltyPoints()),
                () -> assertNotSame(original, copy, "Copy should not be the same in comparison to original"));
    }

    public static Stream<Arguments> parametersForTestEquals() {
        return Stream.of(
            Arguments.of(new StandardItem(1, "Item", 100.0f, "Category", 10), new StandardItem(1, "Item", 100.0f, "Category", 10), true),
            Arguments.of(new StandardItem(1, "Item", 100.0f, "Category", 10), new StandardItem(1, "Item", 100.0f, "Category", 20), false),
            Arguments.of(new StandardItem(1, "Item", 100.0f, "Category", 10), new StandardItem(2, "Item2", 150.0f, "Category1", 10), false),
            Arguments.of(new StandardItem(1, "Item", 100.0f, "Category", 10), null, false));
    }

    @ParameterizedTest
    @MethodSource("parametersForTestEquals")
    public void testEquals(StandardItem a, StandardItem b, boolean expected) {
        assertEquals(expected, a.equals(b));
    }
}