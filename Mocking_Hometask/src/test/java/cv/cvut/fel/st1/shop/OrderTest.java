package cv.cvut.fel.st1.shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void testConstructorWithState() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Item", 0.99f, "Category", 10));
        String customerName = "Name Surname";
        String customerAddress = "Prague Street 123";
        int state = 1;

        Order order = new Order(cart, customerName, customerAddress, state);

        assertAll(
                () -> assertNotNull(order.getItems(), "Items should not be null"),
                () -> assertEquals(customerName, order.getCustomerName(), "Customer name should match"),
                () -> assertEquals(customerAddress, order.getCustomerAddress(), "Customer address should match"),
                () -> assertEquals(state, order.getState(), "State should match and be 1"));
    }

    @Test
    void testConstructorWithoutState() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Item", 1.20f, "Category", 5));
        String customerName = "Name Surname";
        String customerAddress = "Prague Street 123";

        Order order = new Order(cart, customerName, customerAddress);

        assertAll(
                () -> assertNotNull(order.getItems(), "Items should not be null"),
                () -> assertEquals(customerName, order.getCustomerName(), "Customer name should match"),
                () -> assertEquals(customerAddress, order.getCustomerAddress(), "Customer address should match"),
                () -> assertEquals(0, order.getState(), "State should default to 0")
        );
    }

    @Test
    void testNullPointerExceptionThrowOnNullCartWithState() {
        assertThrows(NullPointerException.class, () -> new Order(null, "Name Surname", "Prague 123", 1), "Cart in null");
    }

    @Test
    void testNullPointerExceptionThrowOnNullCart() {
        assertThrows(NullPointerException.class, () -> new Order(null, "Name Surname", "Prague 123"), "Cart in null");
    }

}