package cv.cvut.fel.st1.storage;

import cv.cvut.fel.st1.shop.Item;
import cv.cvut.fel.st1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ItemStockTest {
    static Stream<Arguments> itemProvider() {
        return Stream.of(
                Arguments.of(new StandardItem(1, "Item", 0.99f, "Category", 10)),
                Arguments.of(new StandardItem(2, "Item1", 1.20f, "Category", 5))
        );
    }

    @ParameterizedTest
    @MethodSource("itemProvider")
    void testItemStockConstructor(Item refItem) {
        ItemStock itemStock = new ItemStock(refItem);
        assertEquals(refItem, itemStock.getItem(), "The refItem should match the input");
        assertEquals(0, itemStock.getCount(), "Initial count should be zero");
    }

    @ParameterizedTest
    @CsvSource({
            "0, 5, 5",
            "10, 2, 12",
            "3, 3, 6",
            "1, -1, 0"
    })
    void testIncreaseItemCount(int initialCount, int increment, int expectedCount) {
        Item refItem = new StandardItem(1, "Item", 1.0f, "Test Category", 10);
        ItemStock itemStock = new ItemStock(refItem);
        itemStock.IncreaseItemCount(initialCount);
        itemStock.IncreaseItemCount(increment);
        assertEquals(expectedCount, itemStock.getCount(), "Count after increment should match expected");
    }

    @ParameterizedTest
    @CsvSource({
            "10, 5, 5",
            "10, 2, 8",
            "3, 3, 0",
            "0, 0, 0"
    })
    void testDecreaseItemCount(int initialCount, int decrement, int expectedCount) {
        Item refItem = new StandardItem(1, "Item", 1.0f, "Category", 10);
        ItemStock itemStock = new ItemStock(refItem);
        itemStock.IncreaseItemCount(initialCount);
        itemStock.decreaseItemCount(decrement);
        assertEquals(expectedCount, itemStock.getCount(), "Count after decrement should match expected");
    }

    @Test
    void testDecreaseItemCountThrowsExceptionOnNegativeResult() {
        Item refItem = new StandardItem(1, "Item", 1.0f, "Category", 10);
        ItemStock itemStock = new ItemStock(refItem);
        itemStock.IncreaseItemCount(5);
        assertThrows(IllegalArgumentException.class, () -> itemStock.decreaseItemCount(6), "Cannot decrease stock below zero.");
    }
}