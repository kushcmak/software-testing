package cz.cvut.fel.kushcmak;

public class Factorial {
    public double factorial(int x) {
        if (x == 0)
            return 1;
        return x * factorial(x - 1);
    }

    public void start() {
        factorial(5);
    }
}
