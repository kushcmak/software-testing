package cz.cvut.fel.kushcmak;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FactorialTest {
    Factorial f = new Factorial();
    @Test
    public void factorialTest() {
        Assertions.assertEquals(120, f.factorial(5));
    }

    @Test
    public void factorialTestZero() {
        Assertions.assertEquals(1, f.factorial(0));
    }
}