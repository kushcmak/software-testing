package cz.cvut.fel.kushcmak;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    Calculator c = new Calculator();
    @Test
    public void addTest() {
        Assertions.assertEquals(5, c.add(2, 3));
    }

    @Test
    public void subtractTest() {
        Assertions.assertEquals(2, c.subtract(5, 3));
    }

    @Test
    public void multiplyTest() {
        Assertions.assertEquals(6, c.multiply(2, 3));
    }
    @Test
    public void divisionTest() throws Exception {
        Assertions.assertEquals(2, c.divide(6, 3));
    }
    @Test
    public void exceptionTest_ThrowsException_Exception() throws Exception {
        Assertions.assertThrows(Exception.class, () -> {c.divide(2, 0);}, "Wrong Input");
    }
}