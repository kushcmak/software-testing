package cz.cvut.fel.kushcmak;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class FooTest {
    private Foo foo;

//    @BeforeAll
//    public static void method() {
//        // before all has to be static
//    }

    @BeforeEach
    public void testSetup() {
        foo = new Foo();
    }

    @Test
    @DisplayName("foo.returnNumber returns 5")
    @Order(2)
    public void assert_returnNumber_returnsNumber5() {
        Assertions.assertEquals(5, foo.returnNumber());
    }

    @Test
    @DisplayName("foo.getNum returns 0")
    @Order(1)
    public void assert_getNum_returnsNumber0() {
        Assertions.assertEquals(0, foo.getNum());
    }

    @AfterEach
    public void cleanEnvironment() {
        System.out.println("cleanEnvironment was called");
    }

    @Test
    public void logicalCondition() {
        Assertions.assertTrue(foo.isTrue(true));
    }

    @Disabled("Bug report")
    @Test
    public void logicalConditionFalse() {
        Assertions.assertFalse(foo.isTrue(true));
    }

    @Test
    public void exceptionTest_ThrowsException_Exception() {
        Assertions.assertThrows(Exception.class, () -> {foo.exceptionThrown();});
    }

    @Test
    public void increment() {
        int number = foo.getNum();
        foo.increment();
        int result = foo.getNum();
        Assertions.assertEquals(number+1, result);
    }

    @Test
    public void exceptionThrownByMethod() {
        Assertions.assertThrows(Exception.class, foo::exceptionThrown, "Ocekavana vyjimka");
    }
}