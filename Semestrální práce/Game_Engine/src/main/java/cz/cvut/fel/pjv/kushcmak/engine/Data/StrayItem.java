package cz.cvut.fel.pjv.kushcmak.engine.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * Represents an item that can be found at a specific location in the game world.
 * This class is used for saving and loading item data.
 */
public class StrayItem implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    String name;
    int x;
    int y;

    /**
     * Constructs a StrayItem with the specified name and coordinates.
     *
     * @param name the name of the item
     * @param x    the x-coordinate of the item's location
     * @param y    the y-coordinate of the item's location
     */
    public StrayItem(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }
}
