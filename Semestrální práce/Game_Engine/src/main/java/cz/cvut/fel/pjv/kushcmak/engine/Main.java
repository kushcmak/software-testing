package cz.cvut.fel.pjv.kushcmak.engine;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.GameFrame;

import javax.swing.*;

/**
 * Launches the game.
 */
public class Main {
    public static GameFrame gameFrame;
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            gameFrame = new GameFrame("Adventure");
            gameFrame.setVisible(true);
        });
    }
}