package cz.cvut.fel.pjv.kushcmak.engine.Graphics;

import java.awt.*;

/**
 * Represents a collider in the game world, used for collision detection.
 * Inherits from Rectangle to define its boundaries.
 */
public class Collider extends Rectangle {
    private final boolean isPassThrough;

    /**
     * Constructs a Collider with the specified properties.
     *
     * @param x            the x-coordinate of the collider
     * @param y            the y-coordinate of the collider
     * @param width        the width of the collider
     * @param height       the height of the collider
     * @param isPassThrough whether the collider is pass-through
     */
    public Collider(int x, int y, int width, int height, boolean isPassThrough) {
        super(x, y, width, height);
        this.isPassThrough = isPassThrough;
    }

    /**
     * Checks if the collider is pass-through.
     *
     * @return true if the collider is pass-through, false otherwise
     */
    public boolean isPassThrough() {
        return isPassThrough;
    }
}
