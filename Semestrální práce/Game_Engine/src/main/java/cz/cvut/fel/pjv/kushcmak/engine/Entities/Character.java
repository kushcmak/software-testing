package cz.cvut.fel.pjv.kushcmak.engine.Entities;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Collider;

import java.awt.image.BufferedImage;

/**
 * Base abstract class for any character in the game, including the player, NPCs, and enemies.
 */
public abstract class Character {
    private String name;
    private int health, maxHealth, stamina, maxStamina, attack, baseAttack, armor, baseArmor, money;
    private int x, y, speed;
    public BufferedImage up1, down1, left1, left2, right1, right2;
    private String direction;
    public boolean animationFrame;
    public int animationCounter;
    public Collider hitbox;

    /**
     * Constructs a new Character with the specified attributes.
     *
     * @param name       the name of the character
     * @param health     the current health of the character
     * @param maxHealth  the maximum health of the character
     * @param stamina    the current stamina of the character
     * @param maxStamina the maximum stamina of the character
     * @param attack     the attack points of the character
     * @param baseAttack the base attack points of the character
     * @param armor      the armor points of the character
     * @param baseArmor  the base armor points of the character
     * @param money      the amount of money the character has
     * @param x          the x-coordinate of the character
     * @param y          the y-coordinate of the character
     * @param speed      the movement speed of the character
     */
    public Character(String name, int health, int maxHealth, int stamina, int maxStamina, int attack, int baseAttack, int armor, int baseArmor, int money, int x, int y, int speed) {
        this.name = name;
        this.health = health;
        this.maxHealth = maxHealth;
        this.stamina = stamina;
        this.maxStamina = maxStamina;
        this.attack = attack;
        this.baseAttack = baseAttack;
        this.armor = armor;
        this.baseArmor = baseArmor;
        this.money = money;
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    /**
     * Reduces the character's health by the specified damage amount.
     * If health drops to zero or below, the character dies.
     *
     * @param damage the amount of damage to inflict
     */
    public void takeDamage(int damage) {
        int reducedDamage = damage - this.armor;
        reducedDamage = Math.max(reducedDamage, 0); // Ensure no negative damage
        this.health -= reducedDamage;
        if (this.health <= 0) {
            this.die();
        }
    }

    /**
     * Handles the character's death.
     */
    public void die() {
        System.out.println(name + " has died.");
    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSpeed() {
        return speed;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getMaxStamina() {
        return maxStamina;
    }

    public void setMaxStamina(int maxStamina) {
        this.maxStamina = maxStamina;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public int getBaseAttack() {
        return baseAttack;
    }

    public void setBaseAttack(int baseAttack) {
        this.baseAttack = baseAttack;
    }

    public int getBaseArmor() {
        return baseArmor;
    }

    public void setBaseArmor(int baseArmor) {
        this.baseArmor = baseArmor;
    }

    public Collider getHitbox() {
        return hitbox;
    }
}
