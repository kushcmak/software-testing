package cz.cvut.fel.pjv.kushcmak.engine;

import cz.cvut.fel.pjv.kushcmak.engine.Entities.Enemy;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.EnemyType;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.Villager;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Collider;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Door;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Ramp;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Inventory;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Item;
import cz.cvut.fel.pjv.kushcmak.engine.Items.ItemType;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import static cz.cvut.fel.pjv.kushcmak.engine.Main.gameFrame;

/**
 * Manages all locations in the game. Sets the links between them and defines the way they interact with each other.
 */
public class World {
    private Display display;
    private ArrayList<Collider> colliders;
    private String currentMap;
    private ArrayList<Item> strayItems = new ArrayList<>();
    private ArrayList<Enemy> enemies = new ArrayList<>();
    private ArrayList<Image> images = new ArrayList<>();
    private HashMap<Image, String> itemImages = new HashMap<>();
    private ArrayList<Item> allItems = new ArrayList<>();
    private ArrayList<Villager> villagers = new ArrayList<>();
    private ArrayList<Door> doors = new ArrayList<>();

    /**
     * Constructs a World with the specified colliders, current map, and display.
     *
     * @param colliders  the list of colliders in the world
     * @param currentMap the current map of the world
     * @param display    the display associated with the world
     */
    public World(ArrayList<Collider> colliders, String currentMap, Display display) {
        this.display = display;
        this.colliders = colliders;
        this.currentMap = currentMap;
    }

    /**
     * Initializes all items in the game world.
     */
    public void initAllItems() {
        Item sword = new Item("Sword", ItemType.WEAPON, 10, 0, 0, 0, "A simple sword.", 1,"src/main/resources/Items/Simple Sword.png", display);
        Item helmet = new Item("Helmet", ItemType.HEAD_ARMOR, 0, 10, 0, 0, "A rather strong helmet", 1, "src/main/resources/Items/Helmet.png", display);
        Item steak = new Item("Steak", ItemType.CONSUMABLE, 0, 0, 10, 0, "Delicious.", 1, "src/main/resources/Items/Steak.png", display);
        Item potion = new Item("Potion", ItemType.POTION, 0, 0, 0, 10, "Quite a weird concoction", 1,"src/main/resources/Items/Potion.png", display);
        Item apple = new Item("Apple", ItemType.CONSUMABLE, 0, 0, 5, 0, "A fresh, juicy apple.", 1, "src/main/resources/Items/Apple.png", display);
        Item boots = new Item("Boots", ItemType.BOOTS_ARMOR, 0, 5, 0, 0, "Comfortable boots that offer some protection.", 1, "src/main/resources/Items/Boots.png", display);
        Item chestplate = new Item("Chestplate", ItemType.CHEST_ARMOR, 0, 20, 0, 0, "A heavy chestplate that offers excellent protection.", 1, "src/main/resources/Items/Chestplate.png", display);
        Item flask = new Item("Flask", ItemType.CONSUMABLE, 0, 0, 0, 0, "A small flask for holding liquids.", 1, "src/main/resources/Items/Flask.png", display);
        Item goldenBoots = new Item("Golden Boots", ItemType.BOOTS_ARMOR, 0, 10, 0, 0, "Boots made of gold, offering decent protection.", 1, "src/main/resources/Items/Golden Boots.png", display);
        Item goldenKey = new Item("Golden Key", ItemType.ITEM, 0, 0, 0, 0, "A shiny golden key.", 1, "src/main/resources/Items/Golden Key.png", display);
        Item leggings = new Item("Leggings", ItemType.LEGS_ARMOR, 0, 15, 0, 0, "Leggings made of sturdy material.", 1, "src/main/resources/Items/Leggings.png", display);
        Item metalSword = new Item("Metal Sword", ItemType.WEAPON, 15, 0, 0, 0, "A sword made of durable metal.", 1, "src/main/resources/Items/Metal Sword.png", display);
        Item pants = new Item("Pants", ItemType.LEGS_ARMOR, 0, 10, 0, 0, "Simple pants offering basic protection.", 1, "src/main/resources/Items/Pants.png", display);
        Item robe = new Item("Robe", ItemType.CHEST_ARMOR, 0, 5, 0, 0, "A robe offering minimal protection.", 1, "src/main/resources/Items/Robe.png", display);
        Item shield = new Item("Shield", ItemType.SHIELD, 0, 15, 0, 0, "A sturdy shield for defense.", 1, "src/main/resources/Items/Shield.png", display);
        Item simpleSword = new Item("Simple Sword", ItemType.WEAPON, 10, 0, 0, 0, "A simple sword.", 1, "src/main/resources/Items/Simple Sword.png", display);
        Item stick = new Item("Stick", ItemType.COMPONENT, 5, 0, 0, 0, "A basic wooden stick.", 1, "src/main/resources/Items/Stick.png", display);
        Item stone = new Item("Stone", ItemType.COMPONENT, 0, 0, 0, 0, "A small stone.", 1, "src/main/resources/Items/Stone.png", display);
        Item wizardHat = new Item("Wizard Hat", ItemType.HEAD_ARMOR, 0, 5, 0, 0, "A hat worn by wizards.", 1, "src/main/resources/Items/Wizard Hat.png", display);

        allItems.add(sword);
        allItems.add(helmet);
        allItems.add(steak);
        allItems.add(potion);
        allItems.add(apple);
        allItems.add(boots);
        allItems.add(chestplate);
        allItems.add(flask);
        allItems.add(goldenBoots);
        allItems.add(goldenKey);
        allItems.add(leggings);
        allItems.add(metalSword);
        allItems.add(pants);
        allItems.add(robe);
        allItems.add(shield);
        allItems.add(simpleSword);
        allItems.add(stick);
        allItems.add(stone);
        allItems.add(wizardHat);
    }

    /**
     * Initializes all colliders in the game world based on the current map.
     */
    public void initColliders() {
        if (currentMap.equals("Temple")) {
            Collider templeCollider1 = new Collider(-5, 0, 10, 900, false);
            colliders.add(templeCollider1);
            Collider templeCollider2 = new Collider(0, 780, 570, 100, false);
            colliders.add(templeCollider2);
            Collider templeCollider3 = new Collider(0, -10, 2000, 10, false);
            colliders.add(templeCollider3);
            Collider templeCollider4 = new Collider(1535, 0, 10, 900, false);
            colliders.add(templeCollider4);
            Collider templeCollider5 = new Collider(570, 830, 850, 100, false);
            colliders.add(templeCollider5);
            Collider templeCollider6 = new Collider(0, 655, 130, 125, true);
            colliders.add(templeCollider6);
            Collider templeCollider7 = new Collider(1250, 585, 285, 200, true);
            colliders.add(templeCollider7);
            Ramp templeRamp1 = new Ramp(130, 658, 100, 125, false, 1.0f, true);
            colliders.add(templeRamp1);
            Ramp templeRamp2 = new Ramp(1050, 590, 200, 250, false, 1.0f, false);
            colliders.add(templeRamp2);


            try {
                BufferedImage doorImage = ImageIO.read(new File("src/main/resources/Map/Door.png"));
                Door door = new Door(1405, 500, 50, 200, true, "Village", doorImage);
                doors.add(door);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        } else {
            Collider commonCollider = new Collider(0, 700, 2000, 50, false);
            colliders.add(commonCollider);
            Collider commonCollider1 = new Collider(-5, 0, 10, 900, false);
            colliders.add(commonCollider1);
            Collider commonCollider2 = new Collider(1535, 0, 10, 900, false);
            colliders.add(commonCollider2);
            try {
                BufferedImage doorImage = ImageIO.read(new File("src/main/resources/Map/Door.png"));
                Door door = new Door(200, 500, 20, 100, true, "Temple", doorImage);
                doors.add(door);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Initializes the game world with items and enemies specific to the current map.
     */
    public void initGameWorld() {
        if (currentMap.equals("Temple")) {
            Item sword = new Item("Sword", ItemType.WEAPON, 10, 0, 0, 0, "A simple sword.", 1,"src/main/resources/Items/Simple Sword.png", display);
            Item helmet = new Item("Helmet", ItemType.HEAD_ARMOR, 0, 10, 0, 0, "A rather strong helmet", 1, "src/main/resources/Items/Helmet.png", display);
            Item steak = new Item("Steak", ItemType.CONSUMABLE, 0, 0, 10, 0, "Delicious.", 1, "src/main/resources/Items/Steak.png", display);
            Item potion = new Item("Potion", ItemType.POTION, 0, 0, 0, 10, "Quite a weird concoction", 1,"src/main/resources/Items/Potion.png", display);
            Item goldenKey = new Item("Golden Key", ItemType.ITEM, 0, 0, 0, 0, "A shiny golden key.", 1, "src/main/resources/Items/Golden Key.png", display);
            display.player.getPlayerInventory().addItemToSlot(9, sword);
            display.player.getPlayerInventory().addItemToSlot(10, helmet);
            sword.setX(600);
            sword.setY(300);
            strayItems.add(sword);
            helmet.setX(700);
            helmet.setY(300);
            strayItems.add(helmet);
            steak.setX(500);
            steak.setY(300);
            strayItems.add(steak);
            potion.setX(400);
            potion.setY(300);
            strayItems.add(potion);
            goldenKey.setX(600);
            goldenKey.setY(500);
            strayItems.add(goldenKey);

            Enemy goblin = new Enemy("Goblin", 500, 500, 0, 0, 10, 10, 0, 0, 0, 700, 600, 4, "Don't let cuteness fool you", "Hi", false, true, EnemyType.GOBLIN, display);
            Enemy skeleton = new Enemy("Skeleton", 20, 20, 0, 0, 5, 5, 0, 0, 0, 400, 600, 4, "As good as dead", "...", false, true, EnemyType.SKELETON, display);
            enemies.add(goblin);
            enemies.add(skeleton);
        }
    }

    /**
     * Initializes all images used in the game world, including item images.
     */
    public void initImages() {
        ArrayList<String> paths = initImageList();
        BufferedImage inputImage;
        Image resizedImage;
        for (String path : paths) {
            try {
                inputImage = ImageIO.read(new File(path));
                resizedImage = inputImage.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
                images.add(resizedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ArrayList<String> itemPaths = initItemImageList();

        for (String itemPath : itemPaths) {
            try {
                inputImage = ImageIO.read(new File(itemPath));
                resizedImage = inputImage.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
                itemImages.put(resizedImage, itemPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Initializes all traders in the game world.
     */
    public void initTraders() {
        Villager villager = new Villager("Trader",  20, 20, 20, 20, 5, 5, 0, 0,
                100, 300, 300, 4, "A Trader.", "Hello, wanna buy something?", true, false, new Inventory(), "Deal.", "I am ready whenever the money is.", display);

        villagers.add(villager);
    }

    /**
     * Initializes the list of item image paths.
     *
     * @return an ArrayList of item image paths
     */
    private static ArrayList<String> initItemImageList() {
        ArrayList<String> itemPaths = new ArrayList<>();
        itemPaths.add("src/main/resources/Items/Apple.png");
        itemPaths.add("src/main/resources/Items/Boots.png");
        itemPaths.add("src/main/resources/Items/Chestplate.png");
        itemPaths.add("src/main/resources/Items/Flask.png");
        itemPaths.add("src/main/resources/Items/Golden Boots.png");
        itemPaths.add("src/main/resources/Items/Golden Key.png");
        itemPaths.add("src/main/resources/Items/Helmet.png");
        itemPaths.add("src/main/resources/Items/Leggings.png");
        itemPaths.add("src/main/resources/Items/Metal Sword.png");
        itemPaths.add("src/main/resources/Items/Pants.png");
        itemPaths.add("src/main/resources/Items/Potion.png");
        itemPaths.add("src/main/resources/Items/Robe.png");
        itemPaths.add("src/main/resources/Items/Shield.png");
        itemPaths.add("src/main/resources/Items/Simple Sword.png");
        itemPaths.add("src/main/resources/Items/Steak.png");
        itemPaths.add("src/main/resources/Items/Stick.png");
        itemPaths.add("src/main/resources/Items/Stone.png");
        itemPaths.add("src/main/resources/Items/Wizard Hat.png");
        return itemPaths;
    }

    /**
     * Initializes the list of image paths.
     *
     * @return an ArrayList of image paths
     */
    private static ArrayList<String> initImageList() {
        ArrayList<String> paths = new ArrayList<>();
        paths.add("src/main/resources/Icons/Helmet.png");
        paths.add("src/main/resources/Icons/Chestplate.png");
        paths.add("src/main/resources/Icons/Leggings.png");
        paths.add("src/main/resources/Icons/Boots.png");
        paths.add("src/main/resources/Icons/Sword.png");
        paths.add("src/main/resources/Icons/Shield.png");
        paths.add("src/main/resources/Icons/Apple.png");
        paths.add("src/main/resources/Icons/Potion.png");
        paths.add("src/main/resources/Icons/Pouch.png");
        return paths;
    }

    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public ArrayList<Item> getStrayItems() {
        return strayItems;
    }

    public ArrayList<Collider> getColliders() {
        return colliders;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public HashMap<Image, String> getItemImages() {
        return itemImages;
    }

    public ArrayList<Item> getAllItems() {
        return allItems;
    }

    public ArrayList<Villager> getTraders() {
        return villagers;
    }

    public String getCurrentMap() {
        return currentMap;
    }

    public void setCurrentMap(String currentMap) {
        this.currentMap = currentMap;
    }

    public ArrayList<Door> getDoors() {
        return doors;
    }
}