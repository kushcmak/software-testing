package cz.cvut.fel.pjv.kushcmak.engine.Graphics;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Represents a custom button that can be drawn with an image background and responds to mouse events.
 */
public class DrawnButton {
    private Rectangle bounds;
    private boolean hovered = false;
    private boolean pressed = false;
    private BufferedImage backgroundImage;
    private String text;
    private ActionListener actionListener;

    /**
     * Constructs a DrawnButton with the specified properties.
     *
     * @param text      the text to display on the button
     * @param x         the x-coordinate of the button
     * @param y         the y-coordinate of the button
     * @param width     the width of the button
     * @param height    the height of the button
     * @param imagePath the path to the background image of the button
     */
    public DrawnButton(String text, int x, int y, int width, int height, String imagePath) {
        this.text = text;
        this.bounds = new Rectangle(x, y, width, height);
        try {
            backgroundImage = ImageIO.read(new File(imagePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the action listener for the button.
     *
     * @param listener the action listener to set
     */
    public void setActionListener(ActionListener listener) {
        this.actionListener = listener;
    }

    /**
     * Draws the button on the provided graphics context.
     *
     * @param g the graphics context to draw the button
     */
    public void draw(Graphics2D g) {
        // Draw the button background image
        if (backgroundImage != null) {
            g.drawImage(backgroundImage, bounds.x, bounds.y, bounds.width, bounds.height, null);
        } else {
            // Fallback if image is not available
            if (pressed) {
                g.setColor(Color.GRAY);
            } else if (hovered) {
                g.setColor(Color.LIGHT_GRAY);
            } else {
                g.setColor(Color.WHITE);
            }
            g.fillRoundRect(bounds.x, bounds.y, bounds.width, bounds.height, 30, 30);
        }

        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.BOLD, 16));
        FontMetrics fm = g.getFontMetrics();
        String buttonText = text;
        int textX = bounds.x + (bounds.width - fm.stringWidth(buttonText)) / 2;
        int textY = bounds.y + (bounds.height - fm.getHeight()) / 2 + fm.getAscent();
        g.drawString(buttonText, textX, textY);
    }

    /**
     * Handles mouse events to update the button's state and trigger actions.
     *
     * @param e the mouse event to handle
     */
    public void handleMouseEvent(MouseEvent e) {
        if (bounds.contains(e.getPoint())) {
            if (e.getID() == MouseEvent.MOUSE_PRESSED) {
                pressed = true;
            } else if (e.getID() == MouseEvent.MOUSE_RELEASED) {
                if (pressed && actionListener != null) {
                    actionListener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, text));
                }
                pressed = false;
            } else if (e.getID() == MouseEvent.MOUSE_MOVED) {
                hovered = true;
            }
        } else {
            hovered = false;
        }
    }

    public ActionListener getActionListener() {
        return actionListener;
    }
}
