package cz.cvut.fel.pjv.kushcmak.engine.Items;

/**
 * Enum representing different types of items in the game, each with a description and capacity.
 */
public enum ItemType {
    HEAD_ARMOR("Defensive item for head slot", 1),
    CHEST_ARMOR("Defensive item for chest slot", 1),
    LEGS_ARMOR("Defensive item for legs slot", 1),
    BOOTS_ARMOR("Defensive item for boots slot", 1),
    WEAPON("Increases damage dealt", 1),
    SHIELD("Provides protection", 1),
    CONSUMABLE("Grants temporary benefits", 1),
    POTION("Grants stamina recovery", 1),
    COMPONENT("Can be used in crafts", 1),
    ITEM("Common Item", 1);

    private final String description;
    private final int capacity;

    /**
     * Constructs an ItemType with the specified description and capacity.
     *
     * @param description a brief description of the item type
     * @param capacity    the capacity of the item type
     */
    ItemType(String description, int capacity) {
        this.description = description;
        this.capacity = capacity;
    }

    /**
     * Returns the description of the item type.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the capacity of the item type.
     *
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }
}