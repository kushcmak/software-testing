package cz.cvut.fel.pjv.kushcmak.engine.Entities;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.*;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Inventory;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Item;
import cz.cvut.fel.pjv.kushcmak.engine.Items.ItemType;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Slot;
import cz.cvut.fel.pjv.kushcmak.engine.World;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Class that represents the player character.
 */
public class Player extends Character {
    Display display;
    KeyHandler keyHandler;
    MouseHandler mouseHandler;
    private World world;
    private int level;
    private int experiencePoints;
    private Inventory playerInventory;
    private float verticalVelocity;
    private boolean isJumping = false;
    private final int jumpCooldownDuration = 60;
    private int jumpCooldownTimer;
    private final int inventoryCooldownDuration = 10;
    private int inventoryCooldownTimer;
    private final float gravity = 0.5f;
    private final float jumpPower = -10;
    private final int HITBOX_X_OFFSET = 50;
    private final int HITBOX_Y_OFFSET = 15;
    private int prevX;
    private int prevY;
    private Collider attackArea = new Collider(0, 0, 0, 0, true);
    private ArrayList<Villager> villagers;
    private Villager currentVillager;
    private Door currentDoor;
    private long lastDamageTime = 0;
    private final int damageCooldown = 5000;
    private long attackAnimationStartTime;
    private boolean isAttackAnimationPlaying = false;
    private ArrayList<Door> doors;

    /**
     * Constructs a new player character with the specified attributes.
     *
     * @param name            the name of the player
     * @param health          the current health of the player
     * @param maxHealth       the maximum health of the player
     * @param stamina         the current stamina of the player
     * @param maxStamina      the maximum stamina of the player
     * @param attack          the attack points of the player
     * @param baseAttack      the base attack points of the player
     * @param armor           the armor points of the player
     * @param baseArmor       the base armor points of the player
     * @param money           the amount of money the player has
     * @param x               the x-coordinate of the player
     * @param y               the y-coordinate of the player
     * @param speed           the movement speed of the player
     * @param display         the display object associated with the player
     * @param keyHandler      the key handler for the player
     * @param level           the level of the player
     * @param experiencePoints the experience points of the player
     * @param playerInventory the inventory of the player
     * @param mouseHandler    the mouse handler for the player
     */
    public Player(String name, int health, int maxHealth, int stamina, int maxStamina, int attack, int baseAttack, int armor, int baseArmor, int money, int x, int y, int speed, Display display, KeyHandler keyHandler, int level, int experiencePoints, Inventory playerInventory, MouseHandler mouseHandler) {
        super(name, health, maxHealth, stamina, maxStamina, attack, baseAttack, armor, baseArmor, money, x, y, speed);
        this.display = display;
        this.keyHandler = keyHandler;
        this.level = level;
        this.experiencePoints = experiencePoints;
        this.playerInventory = playerInventory;
        this.jumpCooldownTimer = 0;
        this.mouseHandler = mouseHandler;

        getPlayerView();
        initiate();
        hitbox = new Collider(x + HITBOX_X_OFFSET, y + HITBOX_Y_OFFSET, 50, 125, true);
    }

    /**
     * Initializes the player components.
     */
    private void initiate() {
        world = display.getWorld();
        villagers = display.getWorld().getTraders();
        attackArea.width = 25;
        attackArea.height = 75;
    }

    /**
     * Updates the player's state and handles various actions.
     */
    public void update() {
        prevX = hitbox.x;
        prevY = hitbox.y;

        int currentGameState = display.getGameState();
        int currentSlotRow = display.getSlotRow();
        int currentSlotColumn = display.getSlotColumn();
        int currentTradingSlotRow = display.getTradeSlotRow();
        int currentTradingSlotColumn = display.getTradeSlotColumn();

        boolean inGameState = currentGameState == display.getInGameState();
        boolean customGameState = currentGameState == display.getCustomLevelState();
        boolean inventoryState = currentGameState == display.getInventoryState();
        boolean dialogState = currentGameState == display.getDialogState();
        boolean tradingState = currentGameState == display.getTradingState();

        // Player movement controller actions
        if (keyHandler.w_key_action) {
            if (inGameState || customGameState) {
                this.setDirection("up");
            } else if (inventoryState && currentSlotRow != 0 && inventoryCooldownTimer <= 0) {
                display.setSlotRow(currentSlotRow - 1);
                inventoryCooldownTimer = inventoryCooldownDuration;
            } else if (tradingState && currentTradingSlotRow != 0 && inventoryCooldownTimer <= 0) {
                display.setTradeSlotRow(currentTradingSlotRow - 1);
                inventoryCooldownTimer = inventoryCooldownDuration;
            }
        } else if (keyHandler.s_key_action) {
            if (inGameState || customGameState) {
                this.setDirection("down");
            } else if (inventoryState && currentSlotRow != 3 && inventoryCooldownTimer <= 0) {
                display.setSlotRow(currentSlotRow + 1);
                inventoryCooldownTimer = inventoryCooldownDuration;
            } else if (tradingState && currentTradingSlotRow != 1 && inventoryCooldownTimer <= 0) {
                display.setTradeSlotRow(currentTradingSlotRow + 1);
                inventoryCooldownTimer = inventoryCooldownDuration;
            }
        }

        if (keyHandler.a_key_action) {
            if (inGameState || customGameState) {
                this.setDirection("left");
                int newX = this.getX() - this.getSpeed();
                this.setX(newX);
                this.hitbox.x = newX + HITBOX_X_OFFSET;
            } else if (inventoryState && currentSlotColumn != 0 && inventoryCooldownTimer <= 0) {
                display.setSlotColumn(currentSlotColumn - 1);
                inventoryCooldownTimer = inventoryCooldownDuration;
            } else if (tradingState && currentTradingSlotColumn != 0 && inventoryCooldownTimer <= 0) {
                display.setTradeSlotColumn(currentTradingSlotColumn - 1);
                inventoryCooldownTimer = inventoryCooldownDuration;
            }
        } else if (keyHandler.d_key_action) {
            if (inGameState || customGameState) {
                this.setDirection("right");
                int newX = this.getX() + this.getSpeed();
                this.setX(newX);
                this.hitbox.x = newX + HITBOX_X_OFFSET;
            } else if (inventoryState && currentSlotColumn != 3 && inventoryCooldownTimer <= 0) {
                display.setSlotColumn(currentSlotColumn + 1);
                inventoryCooldownTimer = inventoryCooldownDuration;
            } else if (tradingState && currentTradingSlotColumn != 1 && inventoryCooldownTimer <= 0) {
                display.setTradeSlotColumn(currentTradingSlotColumn + 1);
                inventoryCooldownTimer = inventoryCooldownDuration;
            }
        }


        if (inventoryCooldownTimer > 0) {
            inventoryCooldownTimer--;
        }

        // Jumping mechanics
        if (keyHandler.space_key_action && !isJumping && jumpCooldownTimer <= 0) {
            isJumping = true;
            verticalVelocity = jumpPower;
            jumpCooldownTimer = jumpCooldownDuration;
        }
        if (jumpCooldownTimer > 0) {
            jumpCooldownTimer--;
        }

        // Player UI actions
        if (keyHandler.e_key_action) {
            if (inGameState || customGameState) {
                display.setGameState(display.getInventoryState());
                keyHandler.e_key_action = false;
            } else if (inventoryState) {
                display.setGameState(display.getInGameState());
                keyHandler.e_key_action = false;
            }
        }

        if (keyHandler.q_key_action) {
            if (inGameState || customGameState) {
                pickUpItem(checkItemIntersection());
            } else if (inventoryState) {
                dropItem();
            }
            keyHandler.q_key_action = false;
        }

        if (keyHandler.u_key_action) {
            if ((inGameState || customGameState) && checkDoorCollision()) {
//                useDoor();
                keyHandler.u_key_action = false;
            } else if (inventoryState) {
                useItem();
                keyHandler.u_key_action = false;
            }
        }

        if (keyHandler.t_key_action) {
            if ((inGameState || customGameState) && checkVillagerCollision()) {
                display.setGameState(display.getTradingState());
                keyHandler.t_key_action = false;
            } else if (tradingState) {
                currentVillager = null;
                display.setGameState(display.getInGameState());
                keyHandler.t_key_action = false;
            }
        }

        if (mouseHandler.left_button_action) {
            if (inGameState || customGameState) {
                playAttackAnimation();
                mouseHandler.left_button_action = false;
            }
        }

        if (keyHandler.c_key_action) {
            if (inventoryState) {
                int keyValue = display.getSlotRow() * 4 + display.getSlotColumn() + 1;
                playerInventory.moveItemToFunctionalSlot(keyValue);
                updateArmorAndDefenseStats();
                keyHandler.c_key_action = false;
            }
        }

        if (keyHandler.b_key_action) {
            if (tradingState) {
                int keyValue = display.getTradeSlotRow() * 2 + display.getTradeSlotColumn();
                Item boughtItem = this.getCurrentVillager().getTradeItems().get(keyValue);
                if (playerInventory.hasAvailablePouchSlot()) {
                    this.setMoney(this.getMoney() - boughtItem.getPrice());
                    playerInventory.pickItem(boughtItem);
                }
                keyHandler.b_key_action = false;
            }
        }

        if (keyHandler.r_key_action) {
            if ((inGameState || customGameState) && checkVillagerCollision()) {
                display.setGameState(display.getDialogState());
                keyHandler.r_key_action = false;
            } else if (dialogState) {
                currentVillager = null;
                display.setGameState(display.getInGameState());
                keyHandler.r_key_action = false;
            }
        }

        // Animations
        if (keyHandler.w_key_action || keyHandler.a_key_action || keyHandler.s_key_action || keyHandler.d_key_action) {
            this.animationCounter++;
            if (this.animationCounter > 15) {
                this.animationFrame = !this.animationFrame;
                this.animationCounter = 0;
            }
        }

        // Window controller actions
        if (keyHandler.esc_key_action && currentGameState != display.getMainMenuState()) {
            if (!(display.getGameState() == display.getEscState())) {
                display.setPreviousGameState(currentGameState);
                display.setGameState(display.getEscState());
            }
            keyHandler.esc_key_action = false;
        }

        // Gravity/external effects
        this.setY(this.getY() + (int) verticalVelocity);
        this.hitbox.y = this.hitbox.y + (int) verticalVelocity;
        verticalVelocity += gravity;

        checkForDamage();

        // Collider/ramp collision checks
        for (Collider collider : display.getColliders()) {
            boolean isRamp = collider instanceof Ramp;
            boolean onRamp = isRamp && onRamp((Ramp) collider);
            boolean collisionOccurred = checkCollision(hitbox, collider);

            if (onRamp && !isJumping) {
                adjustPlayerOnRamp(this, (Ramp) collider);
            } else if (collisionOccurred) {
                handleCollision(collider);
                verticalVelocity = 0;
                this.setY(hitbox.y - HITBOX_Y_OFFSET);
            } else if (this.hitbox.y > 800) {
                // Add a death message if a player is below the map, increase limit up to 1000?
                this.setY(800 - HITBOX_Y_OFFSET);
                this.hitbox.y = 800;
                verticalVelocity = 0;
            }
            if (onRamp || (collisionOccurred && collider.getY() >= this.hitbox.y)) {
                isJumping = false;
            }
        }
    }

    private boolean checkDoorCollision() {
        for (Door door : display.getDoors()) {
            if (this.hitbox.intersects(door)) {
                currentDoor = door;
                return true;
            }
        }
        return false;
    }

    private void useDoor() {
        display.getWorld().setCurrentMap(currentDoor.getNextMap());
        display.reinitGameState();
        currentDoor = null;
    }

    /**
     * Checks for collision with a villager.
     *
     * @return true if a collision with a villager occurred, false otherwise
     */
    private boolean checkVillagerCollision() {
        for (Villager villager : villagers) {
            if (this.hitbox.intersects(villager.getTraderHitbox())) {
                currentVillager = villager;
                return true;
            }
        }
        return false;
    }

    /**
     * Plays the attack animation and deals damage to enemies within the attack range.
     */
    private void playAttackAnimation() {
        int attackRange = 50;
        attackArea.x = this.getX();
        attackArea.y = this.getY();

        if (this.getDirection().equals("right")) {
            attackArea.x += attackRange * 2;
        }

        attackArea.width = attackRange;
        attackArea.height = this.hitbox.height;

        ArrayList<Enemy> enemies = display.getWorld().getEnemies();
        ArrayList<Enemy> enemiesToDamage = new ArrayList<>();
        for (Enemy enemy : enemies) {
            if (attackArea.intersects(enemy.getEnemyHitbox())) {
                enemiesToDamage.add(enemy);
            }
        }
        for (Enemy enemy : enemiesToDamage) {
            try {
                enemy.takeDamage(this.getAttack());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        display.getGraphics().setColor(Color.RED);
        display.getGraphics().fillRect(attackArea.x, attackArea.y, attackArea.width, attackArea.height);
        attackAnimationStartTime = System.currentTimeMillis();
        isAttackAnimationPlaying = true;
    }

    /**
     * Checks if the player has taken damage from enemies.
     */
    private void checkForDamage() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastDamageTime < damageCooldown) {
            return;
        }

        ArrayList<Enemy> enemies = display.getWorld().getEnemies();
        for (Enemy enemy : enemies) {
            if (this.hitbox.intersects(enemy.getEnemyHitbox())) {
                this.setHealth(this.getHealth() - enemy.getAttack());
                lastDamageTime = currentTime;
            }
        }
        if (this.getHealth() <= 0) {
            display.setGameState(display.getGameOverState());
            die();
            display.reinitPlayer();
        }
    }

    /**
     * Updates the player's armor and attack stats based on equipped items.
     */
    private void updateArmorAndDefenseStats() {
        int defense = 0;
        int attack = 0;
        Map<Integer, Slot> inventorySlots = playerInventory.getSlots();
        int[] armorSlotIndices = {1, 2, 3, 4};
        for (int index : armorSlotIndices) {
            Slot slot = inventorySlots.get(index);
            if (slot != null && slot.getItem() != null) {
                defense += slot.getItem().getArmorPoints();
            }
        }
        Slot rightHandSlot = inventorySlots.get(5);
        if (rightHandSlot != null && rightHandSlot.getItem() != null) {
            attack += rightHandSlot.getItem().getAttackPoints();
        }
        this.setAttack(this.getBaseAttack() + attack);
        this.setArmor(this.getBaseArmor() + defense);
    }

    /**
     * Updates the player's health and stamina based on the item's effects.
     *
     * @param item the item used by the player
     */
    private void updateHealthAndStamina(Item item) {
        int hpImpact = item.getHpRegen();
        int staminaImpact = item.getStaminaRecovery();
        this.setHealth(Math.min(this.getMaxHealth(), this.getHealth() + hpImpact));
        this.setStamina(Math.min(this.getMaxStamina(), this.getStamina() + staminaImpact));
    }

    /**
     * Checks if the player intersects with any items on the ground.
     *
     * @return the item the player intersects with, or null if no intersection occurs
     */
    private Item checkItemIntersection() {
        for (Item currentItem : display.getStrayItems()) {
            if (this.hitbox.getBounds().intersects(currentItem.getItemHitbox())) {
                return currentItem;
            }
        }
        return null;
    }

    /**
     * Checks for collision between the player's hitbox and a specified collider.
     *
     * @param hitbox   the player's hitbox
     * @param collider the collider to check against
     * @return true if a collision occurs, false otherwise
     */
    private boolean checkCollision(Collider hitbox, Collider collider) {
        return hitbox.getBounds().intersects(collider.getBounds());
    }

    /**
     * Handles the player's collision with a specified collider.
     *
     * @param collider the collider the player collides with
     */
    private void handleCollision(Collider collider) {
        if (!collider.isPassThrough()) {
            hitbox.x = prevX;
            this.setX(prevX - HITBOX_X_OFFSET);
            hitbox.y = prevY;
            this.setY(prevY - HITBOX_Y_OFFSET);
        }
    }

    /**
     * Checks if the player is on a ramp.
     *
     * @param ramp the ramp to check
     * @return true if the player is on the ramp, false otherwise
     */
    private boolean onRamp(Ramp ramp) {
        return hitbox.getBounds().intersects(ramp.getBounds());
    }

    /**
     * Adjusts the player's position on a ramp.
     *
     * @param player the player character
     * @param ramp   the ramp to adjust the player's position on
     */
    private void adjustPlayerOnRamp(Player player, Ramp ramp) {
        int playerFeetX = (int) (player.hitbox.x + player.hitbox.getWidth() / 2);
        int playerFeetY;
        if (ramp.isMirrored()) {
            playerFeetY = (int) (ramp.y + (playerFeetX - ramp.x) * ramp.getSlope());
        } else {
            playerFeetY = (int) (ramp.y + (ramp.x + ramp.getWidth() - playerFeetX) * ramp.getSlope());
        }
        player.hitbox.y = (int) (playerFeetY - player.hitbox.getHeight());
        player.setY(hitbox.y - HITBOX_Y_OFFSET);
    }

    /**
     * Draws the player character on the screen.
     *
     * @param graphics2D the graphics context to use for drawing
     */
    public void draw(Graphics2D graphics2D) {
        BufferedImage currentImage = null;
        switch (this.getDirection()) {
            case "up":
                currentImage = up1;
                break;
            case "down":
                currentImage = down1;
                break;
            case "left":
                if (animationFrame) {
                    currentImage = left1;
                } else {
                    currentImage = left2;
                }
                break;
            case "right":
                if (animationFrame) {
                    currentImage = right1;
                } else {
                    currentImage = right2;
                }
                break;
        }
        graphics2D.drawImage(currentImage, getX(), getY(), 150, 150, null);
    }

    /**
     * Draws the player's hitbox for debugging purposes.
     *
     * @param g the graphics context to use for drawing
     */
    private void drawHitbox(Graphics g) {
        g.setColor(Color.BLUE);
        g.drawRect((int) hitbox.getX(), (int) hitbox.getY(), (int) hitbox.getWidth(), (int) hitbox.getHeight());
    }

    /**
     * Loads the images for the player's character.
     */
    private void getPlayerView() {
        try {
            this.setDirection("down");
            up1 = ImageIO.read(new File("src/main/resources/Characters/Player/Back.png"));
            down1 = ImageIO.read(new File("src/main/resources/Characters/Player/Front.png"));
            left1 = ImageIO.read(new File("src/main/resources/Characters/Player/Left.png"));
            right1 = ImageIO.read(new File("src/main/resources/Characters/Player/Right.png"));

            left2 = ImageIO.read(new File("src/main/resources/Characters/Player/Left2.png"));
            right2 = ImageIO.read(new File("src/main/resources/Characters/Player/Right2.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds an item to the player's inventory. This allows the player to collect items throughout the game.
     *
     * @param item the item to pick up
     */
    private void pickUpItem(Item item) {
        if (item != null) {
            if (this.getPlayerInventory().pickItem(item)) {
                display.getStrayItems().remove(item);
            }
        }
    }

    /**
     * Drops the selected item from the player's inventory.
     */
    private void dropItem() {
        int keyValue = display.getSlotRow() * 4 + display.getSlotColumn() + 1;
        Item item = playerInventory.getSlots().get(keyValue).getItem();
        if (item != null) {
            playerInventory.removeItemFromSlot(keyValue);
            item.setX(this.getX() + HITBOX_X_OFFSET);
            item.setY(this.getY());
            display.getStrayItems().add(item);
        }
    }

    /**
     * Crafts a new item if the player has the necessary components. This method is central to the game's crafting system.
     *
     * @param item the item to craft
     */
    private void craftItem(Item item) {
        // Crafting logic here
    }

    /**
     * Uses an item from the inventory, which could have various effects depending on the item type.
     */
    private void useItem() {
        int keyValue = display.getSlotRow() * 4 + display.getSlotColumn() + 1;
        Item item = playerInventory.getSlots().get(keyValue).getItem();
        ItemType itemType = item.getType();
        if (playerInventory.getSlots().get(keyValue).getItem() != null && (itemType == ItemType.CONSUMABLE || itemType == ItemType.POTION)) {
            playerInventory.removeItemFromSlot(keyValue);
            updateHealthAndStamina(item);
        }
    }

    public Inventory getPlayerInventory() {
        return playerInventory;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExperiencePoints() {
        return experiencePoints;
    }

    public void setExperiencePoints(int experiencePoints) {
        this.experiencePoints = experiencePoints;
    }

    public Villager getCurrentVillager() {
        return currentVillager;
    }

    public Door getCurrentDoor() {
        return currentDoor;
    }

    public void setCurrentVillager(Villager currentVillager) {
        this.currentVillager = currentVillager;
    }


}
