package cz.cvut.fel.pjv.kushcmak.engine.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;

public class DataStorage implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    // Player Stats
    String name;
    int health;
    int maxHealth;
    int stamina;
    int maxStamina;
    int attack;
    int baseAttack;
    int armor;
    int baseArmor;
    int money;
    int level;
    int experiencePoints;

    // Player Inventory
    ArrayList<String> inventoryItems = new ArrayList<>();
    ArrayList<StrayItem> strayItems = new ArrayList<>();
}
