package cz.cvut.fel.pjv.kushcmak.engine.Items;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Collider;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.World;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Base class for items that can be found or used in the game.
 */
public class Item {
    private String name;
    private ItemType type;
    private int attackPoints;
    private int armorPoints;
    private int hpRegen;
    private int staminaRecovery;
    private String description;
    private int price;
    private String imagePath;

    private int x;
    private int y;
    private int width;
    private int height;
    Collider itemHitbox;

    // Gravity
    private float itemVerticalVelocity;
    private final float gravity = 0.5f;
    private World world;
    private ArrayList<Collider> colliders;

    /**
     * Constructs a new Item with the specified attributes.
     *
     * @param name            the name of the item
     * @param type            the type of the item
     * @param attackPoints    the attack points of the item
     * @param armorPoints     the armor points of the item
     * @param hpRegen         the health points regeneration of the item
     * @param staminaRecovery the stamina recovery of the item
     * @param description     the description of the item
     * @param price           the price of the item
     * @param imagePath       the image path of the item
     * @param display         the display object associated with the item
     */
    public Item(String name, ItemType type, int attackPoints, int armorPoints, int hpRegen, int staminaRecovery, String description, int price, String imagePath, Display display) {
        this.name = name;
        this.type = type;
        this.attackPoints = attackPoints;
        this.armorPoints = armorPoints;
        this.hpRegen = hpRegen;
        this.staminaRecovery = staminaRecovery;
        this.description = description;
        this.price = price;
        this.imagePath = imagePath;

        this.width = 50;
        this.height = 50;
        itemHitbox = new Collider(x, y, width, height, true);

        world = display.getWorld();
        colliders = world.getColliders();
    }

    /**
     * Draws the item on the screen using the provided graphics context.
     *
     * @param graphics2D the graphics context to draw the item
     * @param item       the item to be drawn
     * @param itemImages a map of images and their paths
     */
    public void drawItem(Graphics2D graphics2D, Item item, HashMap<Image, String> itemImages) {
        Image drawnImage = null;
        int prevItemX = item.getX();
        int prevItemY = item.getY();
        String imagePath = item.getImagePath();
        if (itemImages.containsValue(imagePath)) {
            drawnImage = getKeyByValue(itemImages, imagePath);
        }
        item.setY(item.getY() + (int) itemVerticalVelocity);
        item.getItemHitbox().y = item.getY() + (int) itemVerticalVelocity;
        itemVerticalVelocity += gravity;
        adjustItems(item, prevItemX, prevItemY);

        graphics2D.drawImage(drawnImage, item.getX(), item.getY(), null);
        //graphics2D.drawRect(item.getItemHitbox().x, item.getItemHitbox().y, (int) item.getItemHitbox().getWidth(), (int) item.getItemHitbox().getWidth()); // testing
    }

    /**
     * Checks if the item collides with the specified collider.
     *
     * @param item     the item to check for collision
     * @param collider the collider to check against
     * @return true if there is a collision, false otherwise
     */
    private boolean checkItemCollision(Item item, Collider collider) {
        return item.getItemHitbox().getBounds().intersects(collider.getBounds());
    }

    /**
     * Adjusts the item's position based on collisions with other colliders.
     *
     * @param item      the item to adjust
     * @param prevItemX the previous X position of the item
     * @param prevItemY the previous Y position of the item
     */
    private void adjustItems(Item item, int prevItemX, int prevItemY) {
        for (Collider collider : colliders) {
            if (checkItemCollision(item, collider)) {
                item.getItemHitbox().x = prevItemX;
                item.setX(prevItemX);
                item.getItemHitbox().y = prevItemY;
                item.setY(prevItemY);
                itemVerticalVelocity = 0;
            }
        }
    }

    /**
     * Retrieves a key from a map by its value.
     *
     * @param map   the map to search
     * @param value the value to find the key for
     * @param <K>   the type of keys in the map
     * @param <V>   the type of values in the map
     * @return the key corresponding to the value, or null if not found
     */
    public static <K, V> K getKeyByValue(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    //    Getters and Setters
    public String getName() {
        return name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public int getPrice() {
        return price;
    }

    public ItemType getType() {
        return type;
    }

    public int getAttackPoints() {
        return attackPoints;
    }

    public int getArmorPoints() {
        return armorPoints;
    }

    public int getHpRegen() {
        return hpRegen;
    }

    public int getStaminaRecovery() {
        return staminaRecovery;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Rectangle getItemHitbox() {
        return itemHitbox;
    }
}