package cz.cvut.fel.pjv.kushcmak.engine.Data;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.Items.*;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles saving and loading game state.
 */
public class Saves {
    private Display display;

    /**
     * Constructs a Saves instance with the specified display.
     *
     * @param display the display associated with the game
     */
    public Saves(Display display) {
        this.display = display;
    }

    /**
     * Saves the current game state to a file.
     */
    public void save() {
        ObjectOutputStream objectOutputStream = null;
        try {
            objectOutputStream = new ObjectOutputStream(new FileOutputStream(new File("src/main/resources/Saves/save.dat")));
            DataStorage dataStorage = new DataStorage();
            // Stats
            dataStorage.name = display.player.getName();
            dataStorage.health = display.player.getHealth();
            dataStorage.maxHealth = display.player.getMaxHealth();
            dataStorage.stamina = display.player.getStamina();
            dataStorage.maxStamina = display.player.getMaxStamina();
            dataStorage.attack = display.player.getAttack();
            dataStorage.baseAttack = display.player.getBaseAttack();
            dataStorage.armor = display.player.getArmor();
            dataStorage.baseArmor = display.player.getBaseArmor();
            dataStorage.money = display.player.getMoney();
            dataStorage.level = display.player.getLevel();
            dataStorage.experiencePoints = display.player.getExperiencePoints();

            // Inventory
            for (Map.Entry<Integer, Slot> entry : display.player.getPlayerInventory().getSlots().entrySet()) {
                Slot slot = entry.getValue();
                String itemName = null;
                if (slot.getItem() != null) {
                    itemName = slot.getItem().getName();
                }
                dataStorage.inventoryItems.add(itemName);
            }

            // Stray Items
            for (Item item : display.getStrayItems()) {
                String strayItemName = item.getName();
                int x = item.getX();
                int y = item.getY();
                dataStorage.strayItems.add(new StrayItem(strayItemName, x, y));
            }

            objectOutputStream.writeObject(dataStorage);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (objectOutputStream != null) {
                try {
                    objectOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Loads the game state from a file.
     */
    public void load() {
        ObjectInputStream objectInputStream = null;
        try {
            File file = new File("src/main/resources/Saves/save.dat");
            if (!file.exists()) {
                System.out.println("Save file not found!");
                return;
            }
            objectInputStream = new ObjectInputStream(new FileInputStream(file));
            DataStorage dataStorage;
            try {
                dataStorage = (DataStorage) objectInputStream.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            display.player.setName(dataStorage.name);
            display.player.setHealth(dataStorage.health);
            display.player.setMaxHealth(dataStorage.maxHealth);
            display.player.setStamina(dataStorage.stamina);
            display.player.setMaxStamina(dataStorage.maxStamina);
            display.player.setAttack(dataStorage.attack);
            display.player.setBaseAttack(dataStorage.baseAttack);
            display.player.setArmor(dataStorage.armor);
            display.player.setBaseArmor(dataStorage.baseArmor);
            display.player.setMoney(dataStorage.money);
            display.player.setLevel(dataStorage.level);
            display.player.setExperiencePoints(dataStorage.experiencePoints);

            display.player.getPlayerInventory().removeAllItems();
            // Load all items
            Inventory inventory = display.player.getPlayerInventory();
            int slotIndex = 1;
            for (String itemName : dataStorage.inventoryItems) {
                if (itemName != null) {
                    Item item = nameToItem(itemName);
                    if (item != null) {
                        inventory.addItemToSlot(slotIndex, item);
                    }
                }
                slotIndex++;
            }

            // Stray Items
            display.getStrayItems().clear();
            for (StrayItem strayItem : dataStorage.strayItems) {
                if (strayItem.name != null) {
                    Item item = nameToItem(strayItem.name);
                    if (item != null) {
                        item.setX(strayItem.x);
                        item.setY(strayItem.y);
                        display.getStrayItems().add(item);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (objectInputStream != null) {
                try {
                    objectInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Converts an item name to an Item object.
     *
     * @param itemName the name of the item
     * @return the Item object corresponding to the item name
     */
    private Item nameToItem(String itemName) {
        Map<String, Item> itemMap = new HashMap<>();
        itemMap.put("Sword", new Item("Sword", ItemType.WEAPON, 10, 0, 0, 0, "A simple sword.", 0,"src/main/resources/Items/Simple Sword.png", display));
        itemMap.put("Helmet", new Item("Helmet", ItemType.HEAD_ARMOR, 0, 10, 0, 0, "A rather strong helmet",0,"src/main/resources/Items/Helmet.png", display));
        itemMap.put("Steak", new Item("Steak", ItemType.CONSUMABLE, 0, 0, 10, 0, "Delicious.",0,"src/main/resources/Items/Steak.png", display));
        itemMap.put("Potion", new Item("Potion", ItemType.POTION, 0, 0, 0, 10, "Quite a weird concoction",0,"src/main/resources/Items/Potion.png", display));
        return itemMap.get(itemName);
    }
}
