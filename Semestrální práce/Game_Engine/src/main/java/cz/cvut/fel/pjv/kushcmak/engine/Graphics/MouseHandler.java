package cz.cvut.fel.pjv.kushcmak.engine.Graphics;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Handles mouse input for the game, tracking the state of mouse buttons.
 */
public class MouseHandler implements MouseListener {
    GameFrame gameFrame;
    Display display;

    // Mouse buttons
    public boolean left_button_action, right_button_action, middle_button_action, right_button_action_performed;

    /**
     * Constructs a MouseHandler with the specified game frame and display.
     *
     * @param gameFrame the game frame to associate with the mouse handler
     * @param display   the display to associate with the mouse handler
     */
    public MouseHandler(GameFrame gameFrame, Display display) {
        this.gameFrame = gameFrame;
        this.display = display;
    }

    /**
     * Invoked when a mouse button has been pressed.
     *
     * @param e the event to be processed
     */
    @Override
    public void mousePressed(MouseEvent e) {
        int button = e.getButton();
        switch (button) {
            case MouseEvent.BUTTON1:
                left_button_action = true;
                break;
            case MouseEvent.BUTTON2:
                middle_button_action = true;
                break;
            case MouseEvent.BUTTON3:
                right_button_action = true;
                right_button_action_performed = true;
                break;
        }
    }

    /**
     * Invoked when a mouse button has been released.
     *
     * @param e the event to be processed
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        int button = e.getButton();
        switch (button) {
            case MouseEvent.BUTTON1:
                left_button_action = false;
                break;
            case MouseEvent.BUTTON2:
                middle_button_action = false;
                break;
            case MouseEvent.BUTTON3:
                right_button_action = false;
                right_button_action_performed = false;
                break;
        }
    }

    /**
     * Invoked when the mouse enters a component.
     *
     * @param e the event to be processed
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        // Method intentionally left blank
    }

    /**
     * Invoked when the mouse exits a component.
     *
     * @param e the event to be processed
     */
    @Override
    public void mouseExited(MouseEvent e) {
        // Method intentionally left blank
    }

    /**
     * Invoked when the mouse button has been clicked (pressed and released) on a component.
     *
     * @param e the event to be processed
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        // Method intentionally left blank
    }
}