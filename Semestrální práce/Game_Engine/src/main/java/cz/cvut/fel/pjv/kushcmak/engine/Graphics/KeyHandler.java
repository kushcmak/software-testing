package cz.cvut.fel.pjv.kushcmak.engine.Graphics;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Handles keyboard input for the game, tracking the state of various keys for different actions.
 */
public class KeyHandler implements KeyListener {
    GameFrame gameFrame;
    Display display;

    // Movement keys
    public boolean w_key_action, a_key_action, s_key_action, d_key_action, space_key_action;
    // Window management keys
    public boolean esc_key_action, esc_action_performed;
    // Inventory keys
    public boolean e_key_action, e_action_performed, u_key_action, u_action_performed, c_key_action, c_action_performed, q_key_action, q_action_performed;
    // Interaction keys
    public boolean r_key_action, r_action_performed, t_key_action, t_action_performed, b_key_action, b_action_performed;

    /**
     * Constructs a KeyHandler with the specified game frame and display.
     *
     * @param gameFrame the game frame to associate with the key handler
     * @param display   the display to associate with the key handler
     */
    public KeyHandler(GameFrame gameFrame, Display display) {
        this.gameFrame = gameFrame;
        this.display = display;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Method intentionally left blank
    }

    /**
     * Invoked when a key has been pressed.
     *
     * @param e the event to be processed
     */
    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        // In-game controls
        switch (code) {
            case KeyEvent.VK_W:
                w_key_action = true;
                break;
            case KeyEvent.VK_A:
                a_key_action = true;
                break;
            case KeyEvent.VK_S:
                s_key_action = true;
                break;
            case KeyEvent.VK_D:
                d_key_action = true;
                break;
            case KeyEvent.VK_SPACE:
                space_key_action = true;
                break;
            case KeyEvent.VK_ESCAPE:
                esc_key_action = true;
                esc_action_performed = true;
                break;
            case KeyEvent.VK_E:
                e_key_action = true;
                e_action_performed = true;
                break;
            case KeyEvent.VK_U:
                u_key_action = true;
                u_action_performed = true;
                break;
            case KeyEvent.VK_C:
                c_key_action = true;
                c_action_performed = true;
                break;
            case KeyEvent.VK_R:
                r_key_action = true;
                r_action_performed = true;
                break;
            case KeyEvent.VK_Q:
                q_key_action = true;
                q_action_performed = true;
                break;
            case KeyEvent.VK_T:
                t_key_action = true;
                t_action_performed = true;
                break;
            case KeyEvent.VK_B:
                b_key_action = true;
                b_action_performed = true;
                break;
        }
    }

    /**
     * Invoked when a key has been released.
     *
     * @param e the event to be processed
     */
    @Override
    public void keyReleased(KeyEvent e) {
        int code = e.getKeyCode();
        switch (code) {
            case KeyEvent.VK_W:
                w_key_action = false;
                break;
            case KeyEvent.VK_A:
                a_key_action = false;
                break;
            case KeyEvent.VK_S:
                s_key_action = false;
                break;
            case KeyEvent.VK_D:
                d_key_action = false;
                break;
            case KeyEvent.VK_SPACE:
                space_key_action = false;
                break;
            case KeyEvent.VK_ESCAPE:
                esc_key_action = false;
                esc_action_performed = false;
                break;
            case KeyEvent.VK_E:
                e_key_action = false;
                e_action_performed = false;
                break;
            case KeyEvent.VK_U:
                u_key_action = false;
                u_action_performed = false;
                break;
            case KeyEvent.VK_C:
                c_key_action = false;
                c_action_performed = false;
                break;
            case KeyEvent.VK_R:
                r_key_action = false;
                r_action_performed = false;
                break;
            case KeyEvent.VK_Q:
                q_key_action = false;
                q_action_performed = false;
                break;
            case KeyEvent.VK_T:
                t_key_action = false;
                t_action_performed = false;
                break;
            case KeyEvent.VK_B:
                b_key_action = false;
                b_action_performed = false;
                break;
        }
    }
}