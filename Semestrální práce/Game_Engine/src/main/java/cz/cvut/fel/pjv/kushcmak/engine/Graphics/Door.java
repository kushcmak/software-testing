package cz.cvut.fel.pjv.kushcmak.engine.Graphics;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Door extends Collider {
    private String nextMap;
    private BufferedImage bufferedImage;

    public Door(int x, int y, int width, int height, boolean isPassThrough, String nextMap, BufferedImage bufferedImage) {
        super(x, y, width, height, isPassThrough);
        this.nextMap = nextMap;
        this.bufferedImage = bufferedImage;
    }

    public void draw(Graphics2D graphics2D) {
        BufferedImage currentImage = this.bufferedImage;
        graphics2D.drawImage(currentImage, (int) getX(), (int) getY(), 150, 150, null);
    }

    public void update(Graphics2D graphics2D) {
        this.draw(graphics2D);
    }

    public String getNextMap() {
        return nextMap;
    }
}
