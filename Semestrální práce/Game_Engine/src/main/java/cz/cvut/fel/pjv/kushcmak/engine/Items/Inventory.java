package cz.cvut.fel.pjv.kushcmak.engine.Items;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages items held by the player.
 */
public class Inventory {
    private Map<Integer, Slot> slots;

    public Inventory() {
        slots = new HashMap<>();
        slots.put(1, new Slot(SlotType.HEAD));
        slots.put(2, new Slot(SlotType.CHEST));
        slots.put(3, new Slot(SlotType.LEGS));
        slots.put(4, new Slot(SlotType.BOOTS));
        slots.put(5, new Slot(SlotType.RIGHT_HAND));
        slots.put(6, new Slot(SlotType.LEFT_HAND));
        slots.put(7, new Slot(SlotType.HEAL));
        slots.put(8, new Slot(SlotType.DRINK));
        slots.put(9, new Slot(SlotType.POUCH));
        slots.put(10, new Slot(SlotType.POUCH));
        slots.put(11, new Slot(SlotType.POUCH));
        slots.put(12, new Slot(SlotType.POUCH));
        slots.put(13, new Slot(SlotType.POUCH));
        slots.put(14, new Slot(SlotType.POUCH));
        slots.put(15, new Slot(SlotType.POUCH));
        slots.put(16, new Slot(SlotType.POUCH));
    }

    public void addItemToSlot(Integer slot_id, Item item) {
        Slot slot = slots.get(slot_id);
        if (slot != null) {
            slot.addItem(item);
        }
    }

    public void removeItemFromSlot(Integer slot_id) {
        Slot slot = slots.get(slot_id);
        if (slot != null) {
            slot.removeItem();
        }
    }

    public boolean isSlotOccupied(Integer slot_id) {
        Slot slot = slots.get(slot_id);
        return slot != null && slot.isOccupied();
    }

    public boolean containsItem(String itemName) {
        for (Slot slot : slots.values()) {
            if (slot.isOccupied() && slot.getItem().getName().equals(itemName)) {
                return true;
            }
        }
        return false;
    }

    public Map<Integer, Slot> getSlots() {
        return slots;
    }

    public boolean pickItem(Item item) {
        for (Map.Entry<Integer, Slot> entry : slots.entrySet()) {
            Slot slot = entry.getValue();
            if (slot.getType() == SlotType.POUCH && !slot.isOccupied()) {
                slot.addItem(item);
                return true;
            }
        }
        return false;
    }

    public boolean hasAvailablePouchSlot() {
        for (Slot slot : slots.values()) {
            if (slot.getType() == SlotType.POUCH && !slot.isOccupied()) {
                return true;
            }
        }
        return false;
    }

    public void moveItemToFunctionalSlot(int fromIndex) {
        Slot currentSlot = slots.get(fromIndex);
        if (currentSlot.getItem() == null) {
            return;
        }
        Item item = currentSlot.getItem();
        for (Slot slot : slots.values()) {
            if (!slot.isOccupied() && slot.canAccept(item)) {
                currentSlot.removeItem();
                slot.addItem(item);
                return;
            }
        }
    }

    public void removeAllItems() {
        for (Slot slot : slots.values()) {
            slot.removeItem();
        }
    }
}
