package cz.cvut.fel.pjv.kushcmak.engine.Items;

/**
 * Represents a slot that can hold an item in an inventory. Each slot has a type that dictates what items it can hold.
 */
public class Slot {
    private final SlotType slotType;
    private Item item;

    /**
     * Constructs a Slot with the specified SlotType.
     *
     * @param slotType the type of the slot
     */
    public Slot(SlotType slotType) {
        this.slotType = slotType;
        this.item = null;
    }

    /**
     * Checks if the slot can accept an item based on its type.
     *
     * @param item the item to check
     * @return true if the item can be accepted, false otherwise
     */
    public boolean canAccept(Item item) {
        return slotType.getAllowedItemTypes().contains(item.getType());
    }

    /**
     * Adds an item to the slot if it is allowed. Replaces any existing item.
     *
     * @param item the item to add
     * @return true if the item was added, false if the item type is not allowed
     */
    public boolean addItem(Item item) {
        if (canAccept(item)) {
            this.item = item;
            return true;
        }
        return false;
    }

    /**
     * Removes the item from the slot and returns it.
     *
     * @return the removed item, or null if the slot was empty
     */
    public Item removeItem() {
        Item removedItem = this.item;
        this.item = null;
        return removedItem;
    }

    /**
     * Checks if the slot is currently occupied by an item.
     *
     * @return true if the slot is occupied, false otherwise
     */
    public boolean isOccupied() {
        return this.item != null;
    }

    /**
     * Returns the item currently in the slot.
     *
     * @return the item in the slot, or null if the slot is empty
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * Returns the type of the slot.
     *
     * @return the slot type
     */
    public SlotType getType() {
        return this.slotType;
    }
}