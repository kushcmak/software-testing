package cz.cvut.fel.pjv.kushcmak.engine.Entities;

/**
 * Enum representing different types of enemies in the game.
 * Each enemy type has specific experience points (XP) and money drop values.
 */
public enum EnemyType {
    GOBLIN(5, 20),
    SKELETON(1, 10);

    private int xpDrop;
    private int moneyDrop;

    /**
     * Constructs an EnemyType with specified XP drop and money drop values.
     *
     * @param xpDrop    the amount of experience points dropped by the enemy
     * @param moneyDrop the amount of money dropped by the enemy
     */
    EnemyType(int xpDrop, int moneyDrop) {
        this.xpDrop = xpDrop;
        this.moneyDrop = moneyDrop;
    }

    /**
     * Returns the amount of experience points dropped by the enemy.
     *
     * @return the XP drop value
     */
    public int getXpDrop() {
        return xpDrop;
    }

    /**
     * Returns the amount of money dropped by the enemy.
     *
     * @return the money drop value
     */
    public int getMoneyDrop() {
        return moneyDrop;
    }
}
