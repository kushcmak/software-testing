package cz.cvut.fel.pjv.kushcmak.engine.Items;

import java.util.EnumSet;

/**
 * Enum representing different types of slots for items, each allowing specific types of items.
 */
public enum SlotType {
    HEAD(EnumSet.of(ItemType.HEAD_ARMOR)),
    CHEST(EnumSet.of(ItemType.CHEST_ARMOR)),
    LEGS(EnumSet.of(ItemType.LEGS_ARMOR)),
    BOOTS(EnumSet.of(ItemType.BOOTS_ARMOR)),
    RIGHT_HAND(EnumSet.of(ItemType.WEAPON)),
    LEFT_HAND(EnumSet.of(ItemType.SHIELD)),
    HEAL(EnumSet.of(ItemType.CONSUMABLE)),
    DRINK(EnumSet.of(ItemType.POTION)),
    POUCH(EnumSet.of(ItemType.CONSUMABLE, ItemType.COMPONENT, ItemType.ITEM, ItemType.WEAPON, ItemType.SHIELD,
            ItemType.HEAD_ARMOR, ItemType.CHEST_ARMOR, ItemType.LEGS_ARMOR, ItemType.BOOTS_ARMOR, ItemType.POTION));

    private final EnumSet<ItemType> allowedItemTypes;

    /**
     * Constructs a SlotType with the specified set of allowed item types.
     *
     * @param allowedItemTypes the set of item types that are allowed in this slot
     */
    SlotType(EnumSet<ItemType> allowedItemTypes) {
        this.allowedItemTypes = allowedItemTypes;
    }

    public EnumSet<ItemType> getAllowedItemTypes() {
        return allowedItemTypes;
    }
}