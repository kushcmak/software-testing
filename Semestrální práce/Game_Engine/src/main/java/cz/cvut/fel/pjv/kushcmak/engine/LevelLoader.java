package cz.cvut.fel.pjv.kushcmak.engine;

import cz.cvut.fel.pjv.kushcmak.engine.Entities.Enemy;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.EnemyType;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.Player;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.Villager;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.*;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Inventory;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Item;
import cz.cvut.fel.pjv.kushcmak.engine.Items.ItemType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class LevelLoader {
    private Display display;
    private World world;

    public LevelLoader(Display display) {
        this.display = display;
        this.world = display.getWorld();
    }

    public void loadLevel(String fileName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = reader.readLine()) != null) {
                parseLine(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseLine(String line) {
        if (line.startsWith("#") || line.isEmpty()) {
            return;
        }

        String[] parts = line.split(",");
        String type = parts[0];

        switch (type) {
            case "player":
                loadPlayer(parts);
                break;
            case "enemy":
                loadEnemy(parts);
                break;
            case "item":
                loadItem(parts);
                break;
            case "collider":
                loadCollider(parts);
                break;
            case "ramp":
                loadRamp(parts);
                break;
            case "door":
                loadDoor(parts);
                break;
            case "villager":
                loadVillager(parts);
                break;
            default:
                throw new IllegalArgumentException("Unknown type: " + type);
        }
    }

    public void loadPlayer(String[] parts) {
        int x = 0, y = 0;
        for (String part : parts) {
            String[] keyValue = part.split("=");
            switch (keyValue[0]) {
                case "x":
                    x = Integer.parseInt(keyValue[1]);
                    break;
                case "y":
                    y = Integer.parseInt(keyValue[1]);
                    break;
            }
        }
        display.player = new Player("Player", 20, 20, 20, 20, 5, 5, 0, 0,
                100, x, y, 4, display, display.getKeyHandler(), 0, 0, new Inventory(), display.getMouseHandler());
    }

    public void loadEnemy(String[] parts) {
        String enemyType = "";
        int x = 0, y = 0;
        for (String part : parts) {
            String[] keyValue = part.split("=");
            switch (keyValue[0]) {
                case "type":
                    enemyType = keyValue[1];
                    break;
                case "x":
                    x = Integer.parseInt(keyValue[1]);
                    break;
                case "y":
                    y = Integer.parseInt(keyValue[1]);
                    break;
            }
        }
        Enemy enemy = createEnemy(enemyType, x, y);
        display.getEnemies().add(enemy);
    }

    public void loadItem(String[] parts) {
        String itemType = "";
        int x = 0, y = 0;
        for (String part : parts) {
            String[] keyValue = part.split("=");
            switch (keyValue[0]) {
                case "type":
                    itemType = keyValue[1];
                    break;
                case "x":
                    x = Integer.parseInt(keyValue[1]);
                    break;
                case "y":
                    y = Integer.parseInt(keyValue[1]);
                    break;
            }
        }
        Item item = createItem(itemType, x, y);
        if (item != null) {
            item.setX(x);
            item.setY(y);
        }
        display.getStrayItems().add(item);
    }

    public void loadCollider(String[] parts) {
        int x = 0, y = 0, width = 0, height = 0;
        for (String part : parts) {
            String[] keyValue = part.split("=");
            switch (keyValue[0]) {
                case "x":
                    x = Integer.parseInt(keyValue[1]);
                    break;
                case "y":
                    y = Integer.parseInt(keyValue[1]);
                    break;
                case "width":
                    width = Integer.parseInt(keyValue[1]);
                    break;
                case "height":
                    height = Integer.parseInt(keyValue[1]);
                    break;
            }
        }
        Collider collider = new Collider(x, y, width, height, false);
        display.getColliders().add(collider);
    }

    public void loadRamp(String[] parts) {
        int x = 0, y = 0, width = 0, height = 0;
        float slope = 0;
        boolean isPassThrough = false, isMirrored = false;
        for (String part : parts) {
            String[] keyValue = part.split("=");
            switch (keyValue[0]) {
                case "x":
                    x = Integer.parseInt(keyValue[1]);
                    break;
                case "y":
                    y = Integer.parseInt(keyValue[1]);
                    break;
                case "width":
                    width = Integer.parseInt(keyValue[1]);
                    break;
                case "height":
                    height = Integer.parseInt(keyValue[1]);
                    break;
                case "slope":
                    slope = Float.parseFloat(keyValue[1]);
                    break;
                case "isPassThrough":
                    isPassThrough = Boolean.parseBoolean(keyValue[1]);
                    break;
                case "isMirrored":
                    isMirrored = Boolean.parseBoolean(keyValue[1]);
                    break;
            }
        }
        Ramp ramp = new Ramp(x, y, width, height, isPassThrough, slope, isMirrored);
        display.getColliders().add(ramp);
    }

    public void loadDoor(String[] parts) {
        int x = 0, y = 0, width = 0, height = 0;
        boolean isPassThrough = false;
        String nextMap = "";
        String imagePath = "";
        for (String part : parts) {
            String[] keyValue = part.split("=");
            switch (keyValue[0]) {
                case "x":
                    x = Integer.parseInt(keyValue[1]);
                    break;
                case "y":
                    y = Integer.parseInt(keyValue[1]);
                    break;
                case "width":
                    width = Integer.parseInt(keyValue[1]);
                    break;
                case "height":
                    height = Integer.parseInt(keyValue[1]);
                    break;
                case "isPassThrough":
                    isPassThrough = Boolean.parseBoolean(keyValue[1]);
                    break;
                case "nextMap":
                    nextMap = keyValue[1];
                    break;
                case "imagePath":
                    imagePath = keyValue[1];
                    break;
            }
        }
        BufferedImage doorImage = loadImage(imagePath);
        Door door = new Door(x, y, width, height, isPassThrough, nextMap, doorImage);
        display.getColliders().add(door);
    }

    public void loadVillager(String[] parts) {
        String name = "", description = "", greeting = "", tradeApproval = "", tradeRejection = "";
        int health = 0, maxHealth = 0, stamina = 0, maxStamina = 0, attack = 0, baseAttack = 0, armor = 0, baseArmor = 0, money = 0, x = 0, y = 0, speed = 0;
        boolean canTrade = false, aggressive = false;
        for (String part : parts) {
            String[] keyValue = part.split("=");
            switch (keyValue[0]) {
                case "name":
                    name = keyValue[1];
                    break;
                case "description":
                    description = keyValue[1];
                    break;
                case "greeting":
                    greeting = keyValue[1];
                    break;
                case "tradeApproval":
                    tradeApproval = keyValue[1];
                    break;
                case "tradeRejection":
                    tradeRejection = keyValue[1];
                    break;
                case "health":
                    health = Integer.parseInt(keyValue[1]);
                    break;
                case "maxHealth":
                    maxHealth = Integer.parseInt(keyValue[1]);
                    break;
                case "stamina":
                    stamina = Integer.parseInt(keyValue[1]);
                    break;
                case "maxStamina":
                    maxStamina = Integer.parseInt(keyValue[1]);
                    break;
                case "attack":
                    attack = Integer.parseInt(keyValue[1]);
                    break;
                case "baseAttack":
                    baseAttack = Integer.parseInt(keyValue[1]);
                    break;
                case "armor":
                    armor = Integer.parseInt(keyValue[1]);
                    break;
                case "baseArmor":
                    baseArmor = Integer.parseInt(keyValue[1]);
                    break;
                case "money":
                    money = Integer.parseInt(keyValue[1]);
                    break;
                case "x":
                    x = Integer.parseInt(keyValue[1]);
                    break;
                case "y":
                    y = Integer.parseInt(keyValue[1]);
                    break;
                case "speed":
                    speed = Integer.parseInt(keyValue[1]);
                    break;
                case "canTrade":
                    canTrade = Boolean.parseBoolean(keyValue[1]);
                    break;
                case "aggressive":
                    aggressive = Boolean.parseBoolean(keyValue[1]);
                    break;
            }
        }
        Villager villager = new Villager(
                name, health, maxHealth, stamina, maxStamina, attack, baseAttack, armor, baseArmor, money, x, y, speed,
                description, greeting, canTrade, aggressive, new Inventory(), tradeApproval, tradeRejection, display
        );
        display.getWorld().getTraders().add(villager);
    }

    private Enemy createEnemy(String type, int x, int y) {
        if (type.equals("skeleton")) {
            return new Enemy("Skeleton", 20, 20, 0, 0, 5, 5, 0, 0, 0, x, y, 4, "As good as dead", "...", false, true, EnemyType.SKELETON, display);
        } else if (type.equals("goblin")) {
            return new Enemy("Goblin", 500, 500, 0, 0, 10, 10, 0, 0, 0, x, y, 4, "Don't let cuteness fool you", "Hi", false, true, EnemyType.GOBLIN, display);
        }
        return null;
    }

    private Item createItem(String type, int x, int y) {
        if (type.equals("sword")) {
            return new Item("Sword", ItemType.WEAPON, 10, 0, 0, 0, "A simple sword.", 2, "src/main/resources/Items/Simple Sword.png", display);
        } else if (type.equals("potion")) {
            return new Item("Potion", ItemType.CONSUMABLE, 0, 0, 0, 10, "A healing potion.", 2, "src/main/resources/Items/Potion.png", display);
        }
        return null;
    }

    private BufferedImage loadImage(String path) {
        try {
            return ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}