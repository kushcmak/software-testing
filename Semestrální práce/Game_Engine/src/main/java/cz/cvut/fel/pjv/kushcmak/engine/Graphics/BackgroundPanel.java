package cz.cvut.fel.pjv.kushcmak.engine.Graphics;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * A custom JPanel for displaying a background image.
 */
public class BackgroundPanel extends JPanel {
    private Image backgroundImage;

    /**
     * Default constructor that sets the background image to a predefined path.
     */
    public BackgroundPanel() {
        setBackgroundImage("src/main/resources/Map/Temple.png");
    }

    /**
     * Constructor that allows setting a custom background image path.
     *
     * @param imagePath the path to the background image
     */
    public BackgroundPanel(String imagePath) {
        setBackgroundImage(imagePath);
    }

    /**
     * Sets the background image from the specified file path.
     *
     * @param imagePath the path to the image file
     */
    public void setBackgroundImage(String imagePath) {
        try {
            backgroundImage = ImageIO.read(new File(imagePath));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        revalidate();
        repaint();
    }

    /**
     * Paints the component with the background image.
     *
     * @param g the Graphics object to protect
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (backgroundImage != null) {
            g.drawImage(backgroundImage, 0, 0, getWidth(), getHeight(), this);
        }
    }
}
