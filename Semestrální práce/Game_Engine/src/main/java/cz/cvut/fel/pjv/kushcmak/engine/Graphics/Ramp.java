package cz.cvut.fel.pjv.kushcmak.engine.Graphics;

/**
 * Represents a ramp in the game world. A ramp is a type of collider with a slope and optional mirroring.
 */
public class Ramp extends Collider {
    private float slope;
    private boolean isMirrored;

    /**
     * Constructs a Ramp with the specified properties.
     *
     * @param x            the x-coordinate of the ramp
     * @param y            the y-coordinate of the ramp
     * @param width        the width of the ramp
     * @param height       the height of the ramp
     * @param isPassThrough whether the ramp is pass-through
     * @param slope        the slope of the ramp
     * @param isMirrored   whether the ramp is mirrored
     */
    public Ramp(int x, int y, int width, int height, boolean isPassThrough, float slope, boolean isMirrored) {
        super(x, y, width, height, isPassThrough);
        this.slope = slope;
        this.isMirrored = isMirrored;
    }

    /**
     * Returns the slope of the ramp.
     *
     * @return the slope of the ramp
     */
    public float getSlope() {
        return slope;
    }

    /**
     * Returns whether the ramp is mirrored.
     *
     * @return true if the ramp is mirrored, false otherwise
     */
    public boolean isMirrored() {
        return isMirrored;
    }
}