package cz.cvut.fel.pjv.kushcmak.engine.Graphics;

import javax.swing.*;
import java.awt.*;

/**
 * Represents the main game frame, handles full screen mode and scene changes.
 */
public class GameFrame extends JFrame {
    public static GraphicsDevice device;

    /**
     * Constructs a GameFrame with the specified title.
     *
     * @param title the title of the game frame
     * @throws HeadlessException if GraphicsEnvironment is headless
     */
    public GameFrame(String title) throws HeadlessException {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        device = env.getDefaultScreenDevice();

        // Card Layout to change scenes
        CardLayout cardLayout = new CardLayout();
        JPanel cardPanel = new JPanel(cardLayout);
        this.setLayout(cardLayout);

        // Game Display
        BackgroundPanel displayPanel = new BackgroundPanel("src/main/resources/Map/TempleLocation.png");
        Display display = new Display();
        display.setOpaque(false);
        displayPanel.add(display);
        cardPanel.add(displayPanel, "displayGamePanel");
        add(cardPanel);
        cardLayout.show(cardPanel, "displayGamePanel");
        enterFullScreen(device);
        display.startGameThread();
        display.requestFocus();
    }

    /**
     * Enters full-screen mode using the specified graphics device.
     *
     * @param device the graphics device to use for full-screen mode
     */
    public void enterFullScreen(GraphicsDevice device) {
        this.setVisible(false);
        this.dispose();
        this.setUndecorated(true);
        this.setResizable(false);
        if (device.isFullScreenSupported()) {
            device.setFullScreenWindow(this);
        } else {
            System.err.println("Full screen not supported");
            this.pack();
        }
    }
}
