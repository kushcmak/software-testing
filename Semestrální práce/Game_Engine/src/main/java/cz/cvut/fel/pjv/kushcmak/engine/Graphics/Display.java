package cz.cvut.fel.pjv.kushcmak.engine.Graphics;

import cz.cvut.fel.pjv.kushcmak.engine.Data.Saves;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.Enemy;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.Player;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.Villager;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Inventory;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Item;
import cz.cvut.fel.pjv.kushcmak.engine.LevelLoader;
import cz.cvut.fel.pjv.kushcmak.engine.World;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Slot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static cz.cvut.fel.pjv.kushcmak.engine.Main.gameFrame;


/**
 * Class which displays the current game state.
 */
public class Display extends JPanel implements Runnable {
    // Screen/Game flow
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int screenWidth = (int) screenSize.getWidth();
    int screenHeight = (int) screenSize.getHeight();
    private final int FPS = 60;
    public Thread gameThread;

    // Player
    public Player player;
    private KeyHandler keyHandler = new KeyHandler(gameFrame, this);
    private MouseHandler mouseHandler = new MouseHandler(gameFrame, this);

    // Game States
    private int gameState;
    private int previousGameState;
    private final int inGameState = 0;
    private final int inventoryState = 1;
    private final int dialogState = 2;
    private final int escState = 3;
    private final int levelEditorState = 4;
    private final int mainMenuState = 5;
    private final int customLevelState = 6;
    private final int tradingState = 7;
    private final int gameOverState = 8;
    private final int winState = 9;

    // Dialog
    private String currentDialog = "Placeholder.";

    // Buttons
    private DrawnButton continueButton;
    private DrawnButton gameplayExitButton;
    private DrawnButton startButton;
    private DrawnButton levelEditor;
    private DrawnButton exitButton;
    private DrawnButton loadButton;
    private DrawnButton loadCustomLevelButton;
    private DrawnButton editLevelButton;

    // UI Drawing helper variables
    private int gameEndtextX;
    private int gameEndtextY;

    // Inventory
    private int slotColumn = 0;
    private int slotRow = 0;
    Inventory inventory = new Inventory();
    private int tradeSlotColumn = 0;
    private int tradeSlotRow = 0;

    // Palette
    Color brightGreen = new Color(162, 234, 146, 175);
    Color darkGreen = new Color(63, 156, 87);
    Color white = new Color(255, 255, 255);

    // World foreign classes
    private World world = new World(new ArrayList<>(), "Temple", this);
    private ArrayList<Collider> colliders;
    private ArrayList<Image> images;
    private HashMap<Image, String> itemImages;
    private ArrayList<Item> strayItems;
    private ArrayList<Enemy> enemies;
    private ArrayList<Villager> villagers;
    private ArrayList<Door> doors;

    // Save
    Saves save = new Saves(this);

    // Level Loader
    private LevelLoader levelLoader;

    /**
     * Constructs the Display, setting up the game window and initiating the game.
     */
    public Display() {
        this.setPreferredSize(new Dimension(screenWidth, screenHeight));
        this.setDoubleBuffered(true);
        this.setLayout(null);
        this.addMouseListener(mouseHandler);
        this.addKeyListener(keyHandler);
        this.setFocusable(true);

        world.initImages();
        gameInitiation();
    }

    /**
     * Initializes the game components and resources.
     */
    public void gameInitiation() {
        reinitPlayer();
        world.initColliders();
        colliders = world.getColliders();
        world.initGameWorld();
        initButtons();
        images = world.getImages();
        itemImages = world.getItemImages();
        strayItems = world.getStrayItems();
        enemies = world.getEnemies();
        world.initAllItems();
        world.initTraders();
        villagers = world.getTraders();
        doors = world.getDoors();
        levelLoader = new LevelLoader(this);
        setGameState(getMainMenuState());
    }

    /**
     * Reinitializes the player with default settings.
     */
    public void reinitPlayer() {
        this.player = new Player("Player", 20, 20, 20, 20, 5, 5, 0, 0,
                100, 20, 300, 4, this, keyHandler, 0, 0, inventory, mouseHandler);
    }

    /**
     * Reinitializes the game state and resources.
     */
    public void reinitGameState() {
        resetGameAssets();
        gameInitiation();
    }

    /**
     * Starts the game loop in a new thread.
     */
    public void startGameThread() {
        gameThread = new Thread(this);
        gameThread.start();
    }

    /**
     * Main game loop, updating and repainting the game at a fixed frame rate.
     */
    @Override
    public void run() {
        double drawInterval = (double) 1000000000 / FPS;
        double nextDrawTime = System.nanoTime() + drawInterval;
        while (gameThread != null) {
            update();
            repaint();
            try {
                double remainingTime = nextDrawTime - System.nanoTime();
                remainingTime = remainingTime / 1000000;
                if (remainingTime < 0) {
                    remainingTime = 0;
                }
                Thread.sleep((long) remainingTime);
                nextDrawTime += drawInterval;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Updates the game state.
     */
    public void update() {
        player.update();
    }

    /**
     * Paints the game components on the screen.
     *
     * @param graphics the graphics context to use for painting
     */
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D graphics2D = (Graphics2D) graphics;

        statesDisplay(graphics2D);

        graphics2D.dispose();
    }

    /**
     * Handles displaying different game states.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void statesDisplay(Graphics2D graphics2D) {
        switch (gameState) {
            case inGameState:
                renderMainGame(graphics2D);
                break;
            case inventoryState:
                drawInventoryScreen(graphics2D);
                break;
            case dialogState:
                drawDialogScreen(graphics2D);
                break;
            case escState:
                continueButton.draw(graphics2D);
                gameplayExitButton.draw(graphics2D);
                break;
            case mainMenuState:
                renderMainMenu(graphics2D);
                break;
            case levelEditorState:
                renderLevelEditor(graphics2D);
                break;
            case customLevelState:
                renderLevelEditorGameplay(graphics2D);
                break;
            case tradingState:
                renderTradingMenu(graphics2D);
                break;
            case gameOverState:
                renderGameOverScreen(graphics2D);
                break;
            case winState:
                renderWinScreen(graphics2D);
                break;
        }
    }

    /**
     * Renders the win screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void renderWinScreen(Graphics2D graphics2D) {
        gameEndScreenSetup(graphics2D);
        graphics2D.drawString("You won.", gameEndtextX, gameEndtextY);
    }

    /**
     * Renders the game over screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void renderGameOverScreen(Graphics2D graphics2D) {
        gameEndScreenSetup(graphics2D);
        graphics2D.drawString("You died.", gameEndtextX, gameEndtextY);
    }


    /**
     * Prepares game end screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void gameEndScreenSetup(Graphics2D graphics2D) {
        int dialogX = screenWidth / 2 - 75;
        int dialogY = screenHeight / 2 - 125;
        int dialogWidth = 150;
        int dialogHeight = 100;

        gameplayExitButton.draw(graphics2D);
        drawUI(dialogX, dialogY, dialogWidth, dialogHeight, graphics2D);

        gameEndtextX = dialogX + 40;
        gameEndtextY = dialogY + dialogHeight / 2;
        graphics2D.setColor(white);
        graphics2D.setFont(new Font("Arial", Font.BOLD, 16));
    }

    /**
     * Renders the trading menu.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void renderTradingMenu(Graphics2D graphics2D) {
        int inventoryX = screenWidth / 2 - screenWidth / 4;
        int inventoryY = screenHeight / 2 - screenHeight / 4;
        int inventoryWidth = screenWidth / 2;
        int inventoryHeight = screenHeight / 2;

        drawUI(inventoryX, inventoryY, inventoryWidth, inventoryHeight, graphics2D);

        drawTradingMenu(graphics2D, inventoryX, inventoryY);
        drawPlayerInventory(graphics2D, inventoryX, inventoryY);
    }

    /**
     * Draws the trading menu.
     *
     * @param graphics2D  the graphics context to use for painting
     * @param inventoryX  the x-coordinate of the inventory
     * @param inventoryY  the y-coordinate of the inventory
     */
    private void drawTradingMenu(Graphics2D graphics2D, int inventoryX, int inventoryY) {
        // Selector initiation
        final int firstSlotX = inventoryX + 100;
        final int firstSlotY = inventoryY + 100;
        int slotX = firstSlotX;
        int slotY = firstSlotY;
        int slotSize = 50;
        int pointerX = firstSlotX + (slotSize * tradeSlotColumn);
        int pointerY = firstSlotY + (slotSize * tradeSlotRow);

        // Icon drawing
        Image drawnImage = null;
        int slotSpacing = 60;
        int tradingSlotCounter = 0;
        if (player.getCurrentVillager() != null) {
            ArrayList<Item> tradeItems = player.getCurrentVillager().getTradeItems();
            for (Item tradeItem : tradeItems) {
                String imagePath = tradeItem.getImagePath();
                if (itemImages.containsValue(imagePath)) {
                    drawnImage = getKeyByValue(itemImages, imagePath);
                }
                graphics2D.drawImage(drawnImage, slotX, slotY, null);
                slotX += slotSpacing;
                if (tradingSlotCounter == 1) {
                    slotX = firstSlotX;
                    slotY += slotSpacing;
                }
                tradingSlotCounter++;
            }
            // Selector
            graphics2D.setColor(white);
            graphics2D.setStroke(new BasicStroke(5));
            int offset = 10;
            graphics2D.drawRoundRect(pointerX + (offset), pointerY + (offset * tradeSlotRow), slotSize, slotSize, 10, 10);
        }
    }

    /**
     * Renders the main menu.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void renderMainMenu(Graphics2D graphics2D) {
        startButton.draw(graphics2D);
        loadButton.draw(graphics2D);
        levelEditor.draw(graphics2D);
        exitButton.draw(graphics2D);
    }

    public void checkGameWin() {
        if (enemies.isEmpty()) {
            setGameState(winState);
        }
    }

    /**
     * Resets the game assets.
     */
    private void resetGameAssets() {
        world.getColliders().clear();
        world.getEnemies().clear();
        world.getStrayItems().clear();
        world.getDoors().clear();
        colliders.clear();
        strayItems.clear();
        enemies.clear();
        doors.clear();
    }

    /**
     * Renders the main game screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void renderMainGame(Graphics2D graphics2D) {
//        drawColliders(graphics2D, colliders);
        player.draw(graphics2D);
        drawTraders(graphics2D);
        drawStrayItems(graphics2D, strayItems);
        drawEnemies(graphics2D);
        checkGameWin();
    }

    /**
     * Renders the level editor screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void renderLevelEditor(Graphics2D graphics2D) {
        editLevelButton.draw(graphics2D);
        loadCustomLevelButton.draw(graphics2D);
    }

    /**
     * Renders the custom level gameplay screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void renderLevelEditorGameplay(Graphics2D graphics2D) {
//        drawColliders(graphics2D, colliders);
        player.draw(graphics2D);
        drawTraders(graphics2D);
        drawStrayItems(graphics2D, strayItems);
        drawEnemies(graphics2D);
        checkGameWin();
    }

    /**
     * Draws the traders on the screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void drawTraders(Graphics2D graphics2D) {
        for (Villager villager : villagers) {
            villager.update(graphics2D);
        }
    }

    /**
     * Draws the enemies on the screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void drawEnemies(Graphics2D graphics2D) {
        for (Enemy enemy : enemies) {
            enemy.update(graphics2D);
        }
    }

    /**
     * Draws the stray items on the screen.
     *
     * @param graphics2D the graphics context to use for painting
     * @param strayItems the list of stray items to draw
     */
    private void drawStrayItems(Graphics2D graphics2D, ArrayList<Item> strayItems) {
        for (Item item : strayItems) {
            item.drawItem(graphics2D, item, itemImages);
        }
    }

    /**
     * Draws the inventory screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void drawInventoryScreen(Graphics2D graphics2D) {
        int inventoryX = screenWidth / 2 - screenWidth / 4;
        int inventoryY = screenHeight / 2 - screenHeight / 4;
        int inventoryWidth = screenWidth / 2;
        int inventoryHeight = screenHeight / 2;

        drawUI(inventoryX, inventoryY, inventoryWidth, inventoryHeight, graphics2D);

        // Player Stats
        drawPlayerStats(graphics2D, inventoryX, inventoryY);
        // Player Inventory
        drawPlayerInventory(graphics2D, inventoryX, inventoryY);
    }

    /**
     * Draws the player's inventory on the screen.
     *
     * @param graphics2D the graphics context to use for painting
     * @param inventoryX the x-coordinate of the inventory
     * @param inventoryY the y-coordinate of the inventory
     */
    private void drawPlayerInventory(Graphics2D graphics2D, int inventoryX, int inventoryY) {
        // Selector initiation
        final int firstSlotX = inventoryX + 500;
        final int firstSlotY = inventoryY + inventoryY / 2;
        int slotX = firstSlotX;
        int slotY = firstSlotY;
        int slotSize = 50;
        int pointerX = firstSlotX + (slotSize * slotColumn);
        int pointerY = firstSlotY + (slotSize * slotRow);

        // Icon drawing
        Image drawnImage;
        int imageCounter = 0;
        int slotSpacing = 60;
        int slotCounter = 0;
        for (Map.Entry<Integer, Slot> entry : player.getPlayerInventory().getSlots().entrySet()) {
            Item currentItem = entry.getValue().getItem();
            if (currentItem != null) {
                String imagePath = currentItem.getImagePath();
                if (itemImages.containsValue(imagePath)) {
                    drawnImage = getKeyByValue(itemImages, imagePath);
                } else {
                    drawnImage = images.get(imageCounter);
                }
            } else {
                drawnImage = images.get(imageCounter);
            }

            graphics2D.drawImage(drawnImage, slotX, slotY, null);
            if (imageCounter < images.size() - 1) {
                imageCounter++;
            } else {
                imageCounter = images.size() - 1;
            }
            slotX += slotSpacing;
            if (slotCounter == 3 || slotCounter == 7 || slotCounter == 11) {
                slotX = firstSlotX;
                slotY += slotSpacing;
            }
            slotCounter++;
        }

        // Selector
        graphics2D.setColor(white);
        graphics2D.setStroke(new BasicStroke(5));
        int offset = 10;
        graphics2D.drawRoundRect(pointerX + (offset * slotColumn), pointerY + (offset * slotRow), slotSize, slotSize, 10, 10);
    }

    /**
     * Draws the player's stats on the screen.
     *
     * @param graphics2D the graphics context to use for painting
     * @param inventoryX the x-coordinate of the inventory
     * @param inventoryY the y-coordinate of the inventory
     */
    private void drawPlayerStats(Graphics2D graphics2D, int inventoryX, int inventoryY) {
        int textX = inventoryX + 50;
        int textY = inventoryY + 80;
        int lineHeight = 40;

        graphics2D.setColor(darkGreen);
        graphics2D.fillRoundRect(textX - 20, textY - 30, 220, 345, 15, 15);

        graphics2D.setColor(white);
        graphics2D.setFont(new Font("Arial", Font.BOLD, 20));
        graphics2D.drawString("Health: " + player.getHealth() + "/" + player.getMaxHealth(), textX, textY);
        textY += lineHeight;
        graphics2D.drawString("Stamina: " + player.getStamina() + "/" + player.getMaxStamina(), textX, textY);
        textY += lineHeight;
        graphics2D.drawString("Attack: " + player.getAttack(), textX, textY);
        textY += lineHeight;
        graphics2D.drawString("Armor: " + player.getArmor(), textX, textY);
        textY += lineHeight;
        graphics2D.drawString("Money: " + player.getMoney(), textX, textY);
        textY += lineHeight;
        graphics2D.drawString("Level: " + player.getLevel(), textX, textY);
        textY += lineHeight;
        graphics2D.drawString("XP: " + player.getExperiencePoints(), textX, textY);
        textY += lineHeight;
        graphics2D.drawString("Speed: " + player.getSpeed(), textX, textY);
    }

    /**
     * Retrieves a key from a map by its value.
     *
     * @param map   the map to search
     * @param value the value to find the key for
     * @param <K>   the type of keys in the map
     * @param <V>   the type of values in the map
     * @return the key corresponding to the value, or null if not found
     */
    public static <K, V> K getKeyByValue(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * Draws the dialog screen.
     *
     * @param graphics2D the graphics context to use for painting
     */
    private void drawDialogScreen(Graphics2D graphics2D) {
        int dialogX = screenWidth / 2 - screenWidth / 4;
        int dialogY = screenHeight / 2 + screenHeight / 5;
        int dialogWidth = screenWidth / 2;
        int dialogHeight = screenHeight / 4;

        drawUI(dialogX, dialogY, dialogWidth, dialogHeight, graphics2D);

        int textX = dialogX + 50;
        int textY = dialogY + 50;
        graphics2D.setColor(white);
        graphics2D.setFont(new Font("Arial", Font.BOLD, 16));

        currentDialog = player.getCurrentVillager().getGreeting();
        // Max amount of symbols on the line = 95
        for (String currentDialog : currentDialog.split("\n")) {
            graphics2D.drawString(currentDialog, textX, textY);
            textY += 30;
        }
    }

    /**
     * Draws a UI panel with rounded corners.
     *
     * @param x         the x-coordinate of the panel
     * @param y         the y-coordinate of the panel
     * @param width     the width of the panel
     * @param height    the height of the panel
     * @param graphics2D the graphics context to use for painting
     */
    public void drawUI(int x, int y, int width, int height, Graphics2D graphics2D) {
        graphics2D.setColor(brightGreen);
        graphics2D.fillRoundRect(x, y, width, height, 20, 20);
        graphics2D.setColor(darkGreen);
        graphics2D.setStroke(new BasicStroke(5));
        graphics2D.drawRoundRect(x, y, width, height, 15, 15);
    }

    /**
     * Initializes the buttons used in the game.
     */
    private void initButtons() {
        continueButton = new DrawnButton("Continue", screenWidth / 2 - 75, screenHeight / 2 - 50, 150, 50, "src/main/resources/Map/Temple.png");
        continueButton.setActionListener(e -> setGameState(previousGameState));

        gameplayExitButton = new DrawnButton("Main Menu", screenWidth / 2 - 75, screenHeight / 2 + 25, 150, 50, "src/main/resources/Map/Temple.png");
        gameplayExitButton.setActionListener(e -> {
            if (gameState != gameOverState && previousGameState != customLevelState) {
                save.save();
            }
            setGameState(mainMenuState);
        });

        startButton = new DrawnButton("Start New Game", screenWidth / 2 - 75, screenHeight / 2 - 125, 150, 50, "src/main/resources/Menu Assets/Spruce1.png");
        startButton.setActionListener(e -> {
            reinitGameState();
            setGameState(inGameState);
        });
        loadButton = new DrawnButton("Load Save", screenWidth / 2 - 75, screenHeight / 2 - 60, 150, 50, "src/main/resources/Menu Assets/Spruce1.png");
        loadButton.setActionListener(e -> {
            setGameState(inGameState);
            save.load();
        });
        levelEditor = new DrawnButton("Level Editor", screenWidth / 2 - 75, screenHeight / 2 + 5, 150, 50, "src/main/resources/Menu Assets/Spruce1.png");
        levelEditor.setActionListener(e -> setGameState(levelEditorState));
        exitButton = new DrawnButton("Exit Game", screenWidth / 2 - 75, screenHeight / 2 + 70, 150, 50, "src/main/resources/Menu Assets/Spruce1.png");
        exitButton.setActionListener(e -> System.exit(0));

        String editorPath = "src/main/resources/Level Editor/level.txt";

        editLevelButton = new DrawnButton("Edit Level", screenWidth / 2 - 75, screenHeight / 2 - 50, 150, 50, "src/main/resources/Menu Assets/Spruce1.png");
        editLevelButton.setActionListener(e -> openTextFile(new File(editorPath)));
        loadCustomLevelButton = new DrawnButton("Load Level", screenWidth / 2 - 75, screenHeight / 2 + 25, 150, 50, "src/main/resources/Menu Assets/Spruce1.png");
        loadCustomLevelButton.setActionListener(e -> {
            setGameState(customLevelState);
            resetGameAssets();
            levelLoader.loadLevel(editorPath);
        });

        // Add mouse listener for button interaction
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                mouseListenerSetup(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                mouseListenerSetup(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                mouseListenerSetup(e);
            }
        });
    }

    /**
     * Handles mouse events for button interaction based on the current game state.
     *
     * @param e the mouse event to handle
     */
    private void mouseListenerSetup(MouseEvent e) {
        switch (gameState) {
            case escState:
                continueButton.handleMouseEvent(e);
                gameplayExitButton.handleMouseEvent(e);
                break;
            case mainMenuState:
                startButton.handleMouseEvent(e);
                loadButton.handleMouseEvent(e);
                levelEditor.handleMouseEvent(e);
                exitButton.handleMouseEvent(e);
                break;
            case levelEditorState:
                editLevelButton.handleMouseEvent(e);
                loadCustomLevelButton.handleMouseEvent(e);
                break;
            case gameOverState, winState:
                gameplayExitButton.handleMouseEvent(e);
                break;
        }
        repaint();
    }

    /**
     * Opens a text file with the default system editor.
     *
     * @param file the file to open
     */
    private void openTextFile(File file) {
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop desktop = Desktop.getDesktop();
                if (file.exists()) {
                    desktop.open(file);
                } else {
                    JOptionPane.showMessageDialog(this, "Level file not found!", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Desktop is not supported!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Draws colliders on the screen.
     *
     * @param g         the graphics context to use for painting
     * @param colliders the list of colliders to draw
     */
    private void drawColliders(Graphics g, ArrayList<Collider> colliders) {
        g.setColor(Color.RED);
        for (Collider coll : colliders) {
            g.drawRect((int) coll.getX(), (int) coll.getY(), (int) coll.getWidth(), (int) coll.getHeight());
        }
    }

    /**
     * Retrieves the list of colliders.
     *
     * @return an ArrayList of Collider objects.
     */
    public ArrayList<Collider> getColliders() {
        return colliders;
    }

    /**
     * Retrieves the current game state.
     *
     * @return the game state as an integer.
     */
    public int getGameState() {
        return gameState;
    }

    /**
     * Sets the current game state.
     *
     * @param gameState the new game state as an integer.
     */
    public void setGameState(int gameState) {
        this.gameState = gameState;
    }

    /**
     * Retrieves the in-game state.
     *
     * @return the in-game state as an integer.
     */
    public int getInGameState() {
        return inGameState;
    }

    /**
     * Retrieves the inventory state.
     *
     * @return the inventory state as an integer.
     */
    public int getInventoryState() {
        return inventoryState;
    }

    /**
     * Retrieves the dialog state.
     *
     * @return the dialog state as an integer.
     */
    public int getDialogState() {
        return dialogState;
    }

    /**
     * Retrieves the escape state.
     *
     * @return the escape state as an integer.
     */
    public int getEscState() {
        return escState;
    }

    /**
     * Retrieves the main menu state.
     *
     * @return the main menu state as an integer.
     */
    public int getMainMenuState() {
        return mainMenuState;
    }

    /**
     * Sets the previous game state.
     *
     * @param previousGameState the previous game state as an integer.
     */
    public void setPreviousGameState(int previousGameState) {
        this.previousGameState = previousGameState;
    }

    /**
     * Retrieves the slot column.
     *
     * @return the slot column as an integer.
     */
    public int getSlotColumn() {
        return slotColumn;
    }

    /**
     * Sets the slot column.
     *
     * @param slotColumn the slot column as an integer.
     */
    public void setSlotColumn(int slotColumn) {
        this.slotColumn = slotColumn;
    }

    /**
     * Retrieves the slot row.
     *
     * @return the slot row as an integer.
     */
    public int getSlotRow() {
        return slotRow;
    }

    /**
     * Sets the slot row.
     *
     * @param slotRow the slot row as an integer.
     */
    public void setSlotRow(int slotRow) {
        this.slotRow = slotRow;
    }

    /**
     * Retrieves the list of stray items.
     *
     * @return an ArrayList of Item objects.
     */
    public ArrayList<Item> getStrayItems() {
        return strayItems;
    }

    /**
     * Retrieves the mouse handler.
     *
     * @return the MouseHandler object.
     */
    public MouseHandler getMouseHandler() {
        return mouseHandler;
    }

    /**
     * Retrieves the world.
     *
     * @return the World object.
     */
    public World getWorld() {
        return world;
    }

    /**
     * Retrieves the list of enemies.
     *
     * @return an ArrayList of Enemy objects.
     */
    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    /**
     * Retrieves the key handler.
     *
     * @return the KeyHandler object.
     */
    public KeyHandler getKeyHandler() {
        return keyHandler;
    }

    /**
     * Retrieves the custom level state.
     *
     * @return the custom level state as an integer.
     */
    public int getCustomLevelState() {
        return customLevelState;
    }

    /**
     * Retrieves the trading state.
     *
     * @return the trading state as an integer.
     */
    public int getTradingState() {
        return tradingState;
    }

    /**
     * Retrieves the game over state.
     *
     * @return the game over state as an integer.
     */
    public int getGameOverState() {
        return gameOverState;
    }

    /**
     * Retrieves the trade slot column.
     *
     * @return the trade slot column as an integer.
     */
    public int getTradeSlotColumn() {
        return tradeSlotColumn;
    }

    /**
     * Sets the trade slot column.
     *
     * @param tradeSlotColumn the trade slot column as an integer.
     */
    public void setTradeSlotColumn(int tradeSlotColumn) {
        this.tradeSlotColumn = tradeSlotColumn;
    }

    /**
     * Retrieves the trade slot row.
     *
     * @return the trade slot row as an integer.
     */
    public int getTradeSlotRow() {
        return tradeSlotRow;
    }

    /**
     * Sets the trade slot row.
     *
     * @param tradeSlotRow the trade slot row as an integer.
     */
    public void setTradeSlotRow(int tradeSlotRow) {
        this.tradeSlotRow = tradeSlotRow;
    }

    /**
     * Retrieves the list of doors.
     *
     * @return an ArrayList of Door objects.
     */
    public ArrayList<Door> getDoors() {
        return doors;
    }

    public DrawnButton getStartButton() {
        return startButton;
    }

    public DrawnButton getExitButton() {
        return exitButton;
    }

    public DrawnButton getLoadButton() {
        return loadButton;
    }

    public DrawnButton getContinueButton() {
        return continueButton;
    }

    public DrawnButton getGameplayExitButton() {
        return gameplayExitButton;
    }

    public DrawnButton getLevelEditor() {
        return levelEditor;
    }

    public DrawnButton getLoadCustomLevelButton() {
        return loadCustomLevelButton;
    }

    public DrawnButton getEditLevelButton() {
        return editLevelButton;
    }

    public int getWinState() {
        return winState;
    }

    public int getLevelEditorState() {
        return levelEditorState;
    }
}
