package cz.cvut.fel.pjv.kushcmak.engine.Entities;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Collider;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Inventory;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Item;
import cz.cvut.fel.pjv.kushcmak.engine.World;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Class that represents the main functions of NPCs in the game.
 */
public class Villager extends NPC {
    private final Inventory traderInventory;
    private final String tradeApproval;
    private final String tradeRejection;
    private Display display;
    private World world;
    private ArrayList<Item> availableTrades;
    private ArrayList<Item> tradeItems = new ArrayList<>();
    private Collider traderHitbox;
    private final int traderHitboxXOffset = 50;
    private final int traderHitboxYOffset = 25;
    private ArrayList<Collider> colliders;
    private float traderVerticalVelocity;
    private final float gravity = 0.5f;

    /**
     * Constructs a Villager with the specified attributes.
     *
     * @param name            the name of the villager
     * @param health          the current health of the villager
     * @param maxHealth       the maximum health of the villager
     * @param stamina         the current stamina of the villager
     * @param maxStamina      the maximum stamina of the villager
     * @param attack          the attack points of the villager
     * @param baseAttack      the base attack points of the villager
     * @param armor           the armor points of the villager
     * @param baseArmor       the base armor points of the villager
     * @param money           the amount of money the villager has
     * @param x               the x-coordinate of the villager
     * @param y               the y-coordinate of the villager
     * @param speed           the movement speed of the villager
     * @param description     a brief description of the villager
     * @param greeting        the greeting message of the villager
     * @param canTrade        whether the villager can trade
     * @param aggressive      whether the villager is aggressive
     * @param traderInventory the inventory of the trader
     * @param tradeApproval   the message displayed on trade approval
     * @param tradeRejection  the message displayed on trade rejection
     * @param display         the display associated with the villager
     */
    public Villager(String name, int health, int maxHealth, int stamina, int maxStamina, int attack, int baseAttack, int armor, int baseArmor, int money, int x, int y, int speed, String description, String greeting, boolean canTrade, boolean aggressive, Inventory traderInventory, String tradeApproval, String tradeRejection, Display display) {
        super(name, health, maxHealth, stamina, maxStamina, attack, baseAttack, armor, baseArmor, money, x, y, speed, description, greeting, canTrade, aggressive);
        this.traderInventory = traderInventory;
        this.tradeApproval = tradeApproval;
        this.tradeRejection = tradeRejection;
        this.display = display;
        this.world = display.getWorld();
        this.availableTrades = world.getAllItems();
        colliders = world.getColliders();
        initializeTrades();
        getTraderView();
        initHitbox();
    }

    /**
     * Updates the villager's state and draws it on the screen.
     *
     * @param graphics2D the graphics context to use for drawing
     */
    public void update(Graphics2D graphics2D) {
        this.performAction();
        this.draw(graphics2D);
    }

    /**
     * Performs the villager's actions, including applying gravity and adjusting position.
     */
    public void performAction() {
        int enemyPrevX = this.getX();
        int enemyPrevY = this.getY();
        applyGravity();
        adjustTraders(enemyPrevX, enemyPrevY);
    }

    /**
     * Applies gravity to the villager.
     */
    private void applyGravity() {
        this.setY(this.getY() + (int) traderVerticalVelocity);
        this.traderHitbox.y = this.traderHitbox.y + (int) traderVerticalVelocity;
        traderVerticalVelocity += gravity;
    }

    /**
     * Adjusts the villager's position based on collisions.
     *
     * @param enemyPrevX the previous x-coordinate of the villager
     * @param enemyPrevY the previous y-coordinate of the villager
     */
    private void adjustTraders(int enemyPrevX, int enemyPrevY) {
        for (Collider collider : colliders) {
            if (checkTraderCollision(this, collider)) {
                this.traderHitbox.x = enemyPrevX + traderHitboxXOffset;
                this.setX(enemyPrevX);
                this.traderHitbox.y = enemyPrevY + traderHitboxYOffset;
                this.setY(enemyPrevY);
                traderVerticalVelocity = 0;
            }
        }
    }

    /**
     * Checks if the villager collides with a specified collider.
     *
     * @param villager the villager to check
     * @param collider the collider to check against
     * @return true if there is a collision, false otherwise
     */
    private boolean checkTraderCollision(Villager villager, Collider collider) {
        return villager.traderHitbox.getBounds().intersects(collider.getBounds());
    }

    /**
     * Initializes the hitbox for the villager.
     */
    private void initHitbox() {
        traderHitbox = new Collider(this.getX() + traderHitboxXOffset, this.getY() + traderHitboxYOffset, 50, 105, true);
    }

    /**
     * Loads the image for the villager.
     */
    private void getTraderView() {
        try {
            this.setDirection("down");
            down1 = ImageIO.read(new File("src/main/resources/Characters/Villagers/Trader.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Draws the villager on the screen.
     *
     * @param graphics2D the graphics context to use for drawing
     */
    public void draw(Graphics2D graphics2D) {
        BufferedImage currentImage = down1;
        graphics2D.drawImage(currentImage, getX(), getY(), 150, 150, null);
    }

    /**
     * Initializes the available trade items for the villager.
     */
    public void initializeTrades() {
        Random random = new Random();
        int counter = 0;
        for (Item ignored : availableTrades) {
            if (counter < 4) {
                int index = random.nextInt(availableTrades.size());
                tradeItems.add(availableTrades.get(index));
                counter++;
            }
        }
    }

    public ArrayList<Item> getTradeItems() {
        return tradeItems;
    }

    public Collider getTraderHitbox() {
        return traderHitbox;
    }
}
