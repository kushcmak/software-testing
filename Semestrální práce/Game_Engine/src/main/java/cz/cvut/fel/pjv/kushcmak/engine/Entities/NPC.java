package cz.cvut.fel.pjv.kushcmak.engine.Entities;

/**
 * Represents a non-player character (NPC) in the game.
 * This class extends the Character class and includes additional attributes and behaviors specific to NPCs.
 */
public abstract class NPC extends Character {
    private String description;
    private String greeting;
    private boolean canTrade;
    private boolean aggressive;

    /**
     * Constructs a new NPC with the specified attributes.
     *
     * @param name        the name of the NPC
     * @param health      the current health of the NPC
     * @param maxHealth   the maximum health of the NPC
     * @param stamina     the current stamina of the NPC
     * @param maxStamina  the maximum stamina of the NPC
     * @param attack      the attack points of the NPC
     * @param baseAttack  the base attack points of the NPC
     * @param armor       the armor points of the NPC
     * @param baseArmor   the base armor points of the NPC
     * @param money       the amount of money the NPC has
     * @param x           the x-coordinate of the NPC
     * @param y           the y-coordinate of the NPC
     * @param speed       the movement speed of the NPC
     * @param description a brief description of the NPC
     * @param greeting    the greeting message of the NPC
     * @param canTrade    whether the NPC can trade
     * @param aggressive  whether the NPC is aggressive
     */
    public NPC(String name, int health, int maxHealth, int stamina, int maxStamina, int attack, int baseAttack, int armor, int baseArmor, int money, int x, int y, int speed, String description, String greeting, boolean canTrade, boolean aggressive) {
        super(name, health, maxHealth, stamina, maxStamina, attack, baseAttack, armor, baseArmor, money, x, y, speed);
        this.description = description;
        this.greeting = greeting;
        this.canTrade = canTrade;
        this.aggressive = aggressive;
    }

    /**
     * Returns the greeting message of the NPC.
     *
     * @return the greeting message
     */
    public String getGreeting() {
        return greeting;
    }
}
