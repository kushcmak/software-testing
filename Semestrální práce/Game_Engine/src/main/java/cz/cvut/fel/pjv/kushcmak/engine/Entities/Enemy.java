package cz.cvut.fel.pjv.kushcmak.engine.Entities;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Collider;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.World;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Represents hostile characters that the player can encounter.
 */
public class Enemy extends NPC {
    private EnemyType enemyType;
    private int xpDrop;
    private int moneyDrop;
    private int actionTimer = 0;
    private final int speed = 4;
    private Collider enemyHitbox;
    private Display display;
    private World world;
    private ArrayList<Collider> colliders;
    private float enemyVerticalVelocity;
    private final float gravity = 0.5f;
    private final int enemyHitboxXOffset = 50;
    private final int enemyHitboxYOffset = 25;

    /**
     * Constructs an Enemy with the specified attributes.
     *
     * @param name        the name of the enemy
     * @param health      the current health of the enemy
     * @param maxHealth   the maximum health of the enemy
     * @param stamina     the current stamina of the enemy
     * @param maxStamina  the maximum stamina of the enemy
     * @param attack      the attack points of the enemy
     * @param baseAttack  the base attack points of the enemy
     * @param armor       the armor points of the enemy
     * @param baseArmor   the base armor points of the enemy
     * @param money       the amount of money the enemy has
     * @param x           the x-coordinate of the enemy
     * @param y           the y-coordinate of the enemy
     * @param speed       the movement speed of the enemy
     * @param description a brief description of the enemy
     * @param greeting    the greeting message of the enemy
     * @param canTrade    whether the enemy can trade
     * @param aggressive  whether the enemy is aggressive
     * @param enemyType   the type of the enemy
     * @param display     the display associated with the enemy
     */
    public Enemy(String name, int health, int maxHealth, int stamina, int maxStamina, int attack, int baseAttack, int armor, int baseArmor, int money, int x, int y, int speed, String description, String greeting, boolean canTrade, boolean aggressive, EnemyType enemyType, Display display) {
        super(name, health, maxHealth, stamina, maxStamina, attack, baseAttack, armor, baseArmor, money, x, y, speed, description, greeting, canTrade, aggressive);
        this.enemyType = enemyType;
        this.xpDrop = enemyType.getXpDrop();
        this.moneyDrop = enemyType.getMoneyDrop();
        this.display = display;

        world = display.getWorld();
        colliders = world.getColliders();

        getEnemyView();
        initHitboxes();
    }

    /**
     * Initializes the enemy's hitboxes.
     */
    private void initHitboxes() {
        enemyHitbox = new Collider(this.getX() + enemyHitboxXOffset, this.getY() + enemyHitboxYOffset, 50, 75, true);
    }

    /**
     * Loads the enemy's images based on its type.
     */
    private void getEnemyView() {
        try {
            this.setDirection("left");
            if (enemyType == EnemyType.GOBLIN) {
                left1 = ImageIO.read(new File("src/main/resources/Characters/Enemies/Goblin Left.png"));
                right1 = ImageIO.read(new File("src/main/resources/Characters/Enemies/Goblin Right.png"));
                left2 = ImageIO.read(new File("src/main/resources/Characters/Enemies/Goblin Left2.png"));
                right2 = ImageIO.read(new File("src/main/resources/Characters/Enemies/Goblin Right2.png"));
            } else if (enemyType == EnemyType.SKELETON) {
                left1 = ImageIO.read(new File("src/main/resources/Characters/Enemies/Skeleton Left.png"));
                right1 = ImageIO.read(new File("src/main/resources/Characters/Enemies/Skeleton Right.png"));
                left2 = ImageIO.read(new File("src/main/resources/Characters/Enemies/Skeleton Left2.png"));
                right2 = ImageIO.read(new File("src/main/resources/Characters/Enemies/Skeleton Right2.png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Draws the enemy on the screen.
     *
     * @param graphics2D the graphics context to use for drawing
     */
    public void draw(Graphics2D graphics2D) {
        BufferedImage currentImage = null;
        switch (this.getDirection()) {
            case "left":
                if (this.animationFrame) {
                    currentImage = left1;
                } else {
                    currentImage = left2;
                }
                break;
            case "right":
                if (this.animationFrame) {
                    currentImage = right1;
                } else {
                    currentImage = right2;
                }
                break;
        }
        graphics2D.drawImage(currentImage, getX(), getY(), 150, 150, null);
    }

    /**
     * Updates the enemy's state and draws it on the screen.
     *
     * @param graphics2D the graphics context to use for drawing
     */
    public void update(Graphics2D graphics2D) {
        this.performAction();
        this.draw(graphics2D);
    }

    /**
     * Performs the enemy's actions, including movement and gravity application.
     */
    public void performAction() {
        this.animationCounter++;
        if (this.animationCounter > 15) {
            this.animationFrame = !this.animationFrame;
            this.animationCounter = 0;
        }
        actionTimer++;
        if (actionTimer >= 60) {
            Random random = new Random();
            int i = random.nextInt(100) + 1;
            if (i <= 50) {
                this.setDirection("right");
                this.setX(this.getX() + speed);
            }
            if (i > 50) {
                this.setDirection("left");
                this.setX(this.getX() - speed);
            }
            actionTimer = 0;
        }
        int enemyPrevX = this.getX();
        int enemyPrevY = this.getY();
        applyGravity();
        adjustEnemies(enemyPrevX, enemyPrevY);
    }

    /**
     * Applies gravity to the enemy.
     */
    private void applyGravity() {
        this.setY(this.getY() + (int) enemyVerticalVelocity);
        this.enemyHitbox.y = this.enemyHitbox.y + (int) enemyVerticalVelocity;
        enemyVerticalVelocity += gravity;
    }

    /**
     * Adjusts the enemy's position based on collisions.
     *
     * @param enemyPrevX the previous x-coordinate of the enemy
     * @param enemyPrevY the previous y-coordinate of the enemy
     */
    private void adjustEnemies(int enemyPrevX, int enemyPrevY) {
        for (Collider collider : colliders) {
            if (checkEnemyCollision(this, collider)) {
                this.enemyHitbox.x = enemyPrevX + enemyHitboxXOffset;
                this.setX(enemyPrevX);
                this.enemyHitbox.y = enemyPrevY + enemyHitboxYOffset;
                this.setY(enemyPrevY);
                enemyVerticalVelocity = 0;
            }
        }
    }

    /**
     * Checks if the enemy collides with a specified collider.
     *
     * @param enemy    the enemy to check
     * @param collider the collider to check against
     * @return true if there is a collision, false otherwise
     */
    private boolean checkEnemyCollision(Enemy enemy, Collider collider) {
        return enemy.enemyHitbox.getBounds().intersects(collider.getBounds());
    }

    /**
     * Reduces the enemy's health by the specified damage amount and removes the enemy if health reaches zero.
     *
     * @param damage the amount of damage to inflict
     */
    public void takeDamage(int damage) {
        int reducedDamage = damage - this.getArmor();
        reducedDamage = Math.max(reducedDamage, 0);
        this.setHealth(this.getHealth() - reducedDamage);
        if (this.getHealth() <= 0) {
            display.getEnemies().remove(this);
            display.player.setMoney(display.player.getMoney() + this.moneyDrop);
            display.player.setExperiencePoints(display.player.getExperiencePoints() + this.xpDrop);
            die();
        }
    }

    // Getters and Setters
    public Collider getEnemyHitbox() {
        return enemyHitbox;
    }

    public void setActionTimer(int actionTimer) {
        this.actionTimer = actionTimer;
    }
}
