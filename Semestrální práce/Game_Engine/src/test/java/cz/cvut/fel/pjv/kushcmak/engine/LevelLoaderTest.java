package cz.cvut.fel.pjv.kushcmak.engine;

import cz.cvut.fel.pjv.kushcmak.engine.Entities.Enemy;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.Villager;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Collider;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Door;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Ramp;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class LevelLoaderTest {

    private Display displayMock;
    private World worldMock;
    private ArrayList<Enemy> enemiesMock;
    private ArrayList<Item> strayItemsMock;
    private ArrayList<Collider> collidersMock;
    private ArrayList<Villager> tradersMock;
    private LevelLoader levelLoader;

    @BeforeEach
    void setUp() {
        displayMock = mock(Display.class);
        worldMock = mock(World.class);
        enemiesMock = mock(ArrayList.class);
        strayItemsMock = mock(ArrayList.class);
        collidersMock = mock(ArrayList.class);
        tradersMock = mock(ArrayList.class);

        when(displayMock.getWorld()).thenReturn(worldMock);
        when(displayMock.getEnemies()).thenReturn(enemiesMock);
        when(displayMock.getStrayItems()).thenReturn(strayItemsMock);
        when(displayMock.getColliders()).thenReturn(collidersMock);
        when(worldMock.getTraders()).thenReturn(tradersMock);

        levelLoader = spy(new LevelLoader(displayMock));
    }

    @Test
    void testLoadPlayer() {
        String[] parts = {"player", "x=10", "y=20"};
        levelLoader.loadPlayer(parts);
        assertEquals(10, displayMock.player.getX());
        assertEquals(20, displayMock.player.getY());
    }

    @Test
    void testLoadEnemy() {
        String[] parts = {"enemy", "type=skeleton", "x=30", "y=40"};
        levelLoader.loadEnemy(parts);
        verify(enemiesMock).add(any(Enemy.class));
    }

    @Test
    void testLoadItem() {
        String[] parts = {"item", "type=sword", "x=50", "y=60"};
        levelLoader.loadItem(parts);
        verify(strayItemsMock).add(any(Item.class));
    }

    @Test
    void testLoadCollider() {
        String[] parts = {"collider", "x=10", "y=20", "width=30", "height=40"};
        levelLoader.loadCollider(parts);
        verify(collidersMock).add(any(Collider.class));
    }

    @Test
    void testLoadRamp() {
        String[] parts = {"ramp", "x=10", "y=20", "width=30", "height=40", "slope=1.5", "isPassThrough=true", "isMirrored=false"};
        levelLoader.loadRamp(parts);
        verify(collidersMock).add(any(Ramp.class));
    }

    @Test
    void testLoadDoor() {
        String[] parts = {"door", "x=10", "y=20", "width=30", "height=40", "isPassThrough=true", "nextMap=nextLevel", "imagePath=src/main/resources/Map/Door.png"};
        levelLoader.loadDoor(parts);
        verify(collidersMock).add(any(Door.class));
    }

    @Test
    void testLoadVillager() {
        String[] parts = {
                "villager", "name=John", "description=A friendly villager", "greeting=Hello",
                "tradeApproval=Approved", "tradeRejection=Rejected", "health=100", "maxHealth=100",
                "stamina=50", "maxStamina=50", "attack=10", "baseAttack=10", "armor=5", "baseArmor=5",
                "money=20", "x=10", "y=20", "speed=3", "canTrade=true", "aggressive=false"
        };
        levelLoader.loadVillager(parts);
        verify(tradersMock).add(any(Villager.class));
    }
}