package cz.cvut.fel.pjv.kushcmak.engine.ProcessTests;

import cz.cvut.fel.pjv.kushcmak.engine.Entities.Player;
import cz.cvut.fel.pjv.kushcmak.engine.Entities.Villager;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.DrawnButton;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.KeyHandler;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.MouseHandler;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static cz.cvut.fel.pjv.kushcmak.engine.Main.gameFrame;

public class ProcessTests {
    private Display display;
    private Player player;
    private KeyHandler keyHandler;

    @BeforeEach
    public void setUp() {
        display = new Display();
        keyHandler = new KeyHandler(gameFrame, display);
        player = new Player("Player", 20, 20, 20, 20, 5, 5, 0, 0, 100, 20, 300, 4, display, keyHandler, 0, 0, new Inventory(), new MouseHandler(gameFrame, display));
    }

    @Test
    public void testSequence1() {
        // Test sequence: 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 9 - 10 - 18 - 25
        // Test sequence: A - B - C - D - E - F - G - K - L - H - P - Q

        display.gameInitiation();
        display.startGameThread();
        assertEquals(display.getMainMenuState(), display.getGameState());

        clickButton(display.getStartButton());
        assertEquals(display.getInGameState(), display.getGameState());

        performAction(keyHandler, KeyEvent.VK_E);
        assertEquals(display.getInventoryState(), display.getGameState());

        performAction(keyHandler, KeyEvent.VK_E);
        assertEquals(display.getInGameState(), display.getGameState());

        performAction(keyHandler, KeyEvent.VK_ESCAPE);
        assertEquals(display.getEscState(), display.getGameState());

        clickButton(display.getGameplayExitButton());
        assertEquals(display.getMainMenuState(), display.getGameState());

        // clickButton(display.getExitButton());
    }

    @Test
    public void testSequence2() {
        // Test sequence: 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 11 - 19 - 24 - 8 - 14 - 22 - 25
        // Test sequence: A - B - C - D - E - F - G - K - H - M - P - K - H - P - Q
        display.gameInitiation();
        display.startGameThread();
        assertEquals(display.getMainMenuState(), display.getGameState());

        clickButton(display.getStartButton());
        assertEquals(display.getInGameState(), display.getGameState());

        Villager villager = new Villager("",20, 20, 20, 20, 5, 5, 0, 0, 100, 20, 300, 4, "","", true, false, new Inventory(),"","", display);
        display.getWorld().getTraders().add(villager);
        player.getHitbox().setBounds(villager.getX(), villager.getY(), 100, 100);
        player.setCurrentVillager(villager);
        performAction(keyHandler, KeyEvent.VK_T);
        assertEquals(display.getTradingState(), display.getGameState());
        performAction(keyHandler, KeyEvent.VK_T);

        performAction(keyHandler, KeyEvent.VK_E);
        assertEquals(display.getInventoryState(), display.getGameState());

        performAction(keyHandler, KeyEvent.VK_E);
        assertEquals(display.getInGameState(), display.getGameState());

        performAction(keyHandler, KeyEvent.VK_ESCAPE);
        assertEquals(display.getEscState(), display.getGameState());

        clickButton(display.getGameplayExitButton());
        assertEquals(display.getMainMenuState(), display.getGameState());

        clickButton(display.getLevelEditor());
        assertEquals(display.getLevelEditorState(), display.getGameState());

        clickButton(display.getLoadCustomLevelButton());
        assertEquals(display.getCustomLevelState(), display.getGameState());

        player.setHealth(-2);
        player.update();
        assertEquals(display.getGameOverState(), display.getGameState());
        player.setHealth(10);

        clickButton(display.getGameplayExitButton());
        assertEquals(display.getMainMenuState(), display.getGameState());

        clickButton(display.getStartButton());
        assertEquals(display.getInGameState(), display.getGameState());

        display.getEnemies().clear();
        display.checkGameWin();
        assertEquals(display.getWinState(), display.getGameState());

        performAction(keyHandler, KeyEvent.VK_ESCAPE);
        assertEquals(display.getEscState(), display.getGameState());

        clickButton(display.getGameplayExitButton());
        assertEquals(display.getMainMenuState(), display.getGameState());

        // clickButton(display.getExitButton());
    }

    @Test
    public void testSequence3() {
        // Test sequence: 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 26
        // Test sequence: A - B - C - D - E - F - G - K - Q

        display.gameInitiation();
        display.startGameThread();
        assertEquals(display.getMainMenuState(), display.getGameState());

        // clickButton(display.getExitButton());
    }

    @Test
    public void testSequence4() {
        // Test sequence: 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 12 - 20 - 25
        // Test sequence: A - B - C - D - E - F - G - K - H - N - P - Q
        display.gameInitiation();
        display.startGameThread();
        assertEquals(display.getMainMenuState(), display.getGameState());

        clickButton(display.getStartButton());
        assertEquals(display.getInGameState(), display.getGameState());

        player.setHealth(-2);
        player.update();
        assertEquals(display.getGameOverState(), display.getGameState());
        player.setHealth(10);

        performAction(keyHandler, KeyEvent.VK_ESCAPE);
        assertEquals(display.getEscState(), display.getGameState());

        clickButton(display.getGameplayExitButton());
        assertEquals(display.getMainMenuState(), display.getGameState());

        // clickButton(display.getExitButton());
    }

    @Test
    public void testSequence5() {
        // Test sequence: 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 15 - 23 - 25
        // Test sequence: A - B - C - D - E - F - G - K - H - O - P - Q
        display.gameInitiation();
        display.startGameThread();
        assertEquals(display.getMainMenuState(), display.getGameState());

        clickButton(display.getStartButton());
        assertEquals(display.getInGameState(), display.getGameState());

        display.getEnemies().clear();
        display.checkGameWin();
        assertEquals(display.getWinState(), display.getGameState());

        performAction(keyHandler, KeyEvent.VK_ESCAPE);
        assertEquals(display.getEscState(), display.getGameState());

        clickButton(display.getGameplayExitButton());
        assertEquals(display.getMainMenuState(), display.getGameState());

        // clickButton(display.getExitButton());
    }

    @Test
    public void testSequence6() {
        // Test sequence: 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 13 - 21 - 25
        // Test sequence: A - B - C - D - E - F - G - K - H - J - P - Q
        display.gameInitiation();
        display.startGameThread();
        assertEquals(display.getMainMenuState(), display.getGameState());

        clickButton(display.getStartButton());
        assertEquals(display.getInGameState(), display.getGameState());

        Villager villager = new Villager("",20, 20, 20, 20, 5, 5, 0, 0, 100, 20, 300, 4, "","", true, false, new Inventory(),"","", display);
        display.getWorld().getTraders().add(villager);
        player.getHitbox().setBounds(villager.getX(), villager.getY(), 100, 100);
        player.setCurrentVillager(villager);
        performAction(keyHandler, KeyEvent.VK_R);
        assertEquals(display.getDialogState(), display.getGameState());
        performAction(keyHandler, KeyEvent.VK_R);

        performAction(keyHandler, KeyEvent.VK_ESCAPE); // Esc state
        assertEquals(display.getEscState(), display.getGameState());

        clickButton(display.getGameplayExitButton());
        assertEquals(display.getMainMenuState(), display.getGameState());

        // clickButton(display.getExitButton());
    }

    @Test
    public void testSequence7() {
        // Test sequence: 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 9 - 17 - 25
        // Test sequence: A - B - C - D - E - F - G - K - L - P - Q
        display.gameInitiation();
        display.startGameThread();
        assertEquals(display.getMainMenuState(), display.getGameState());

        clickButton(display.getLevelEditor());
        assertEquals(display.getLevelEditorState(), display.getGameState());

        performAction(keyHandler, KeyEvent.VK_ESCAPE); // Esc state
        assertEquals(display.getEscState(), display.getGameState());

        clickButton(display.getGameplayExitButton());
        assertEquals(display.getMainMenuState(), display.getGameState());

        // clickButton(display.getExitButton());
    }

    private void performAction(KeyHandler keyHandler, int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_W:
                keyHandler.w_key_action = true;
                player.update();
                keyHandler.w_key_action = false;
                break;
            case KeyEvent.VK_A:
                keyHandler.a_key_action = true;
                player.update();
                keyHandler.a_key_action = false;
                break;
            case KeyEvent.VK_S:
                keyHandler.s_key_action = true;
                player.update();
                keyHandler.s_key_action = false;
                break;
            case KeyEvent.VK_D:
                keyHandler.d_key_action = true;
                player.update();
                keyHandler.d_key_action = false;
                break;
            case KeyEvent.VK_SPACE:
                keyHandler.space_key_action = true;
                player.update();
                keyHandler.space_key_action = false;
                break;
            case KeyEvent.VK_ESCAPE:
                keyHandler.esc_key_action = true;
                player.update();
                keyHandler.esc_key_action = false;
                break;
            case KeyEvent.VK_E:
                keyHandler.e_key_action = true;
                player.update();
                keyHandler.e_key_action = false;
                break;
            case KeyEvent.VK_U:
                keyHandler.u_key_action = true;
                player.update();
                keyHandler.u_key_action = false;
                break;
            case KeyEvent.VK_C:
                keyHandler.c_key_action = true;
                player.update();
                keyHandler.c_key_action = false;
                break;
            case KeyEvent.VK_R:
                keyHandler.r_key_action = true;
                player.update();
                keyHandler.r_key_action = false;
                break;
            case KeyEvent.VK_Q:
                keyHandler.q_key_action = true;
                player.update();
                keyHandler.q_key_action = false;
                break;
            case KeyEvent.VK_T:
                keyHandler.t_key_action = true;
                player.update();
                keyHandler.t_key_action = false;
                break;
            case KeyEvent.VK_B:
                keyHandler.b_key_action = true;
                player.update();
                keyHandler.b_key_action = false;
                break;
        }
    }

    private void clickButton(DrawnButton button) {
        button.getActionListener().actionPerformed(new ActionEvent(button, ActionEvent.ACTION_PERFORMED, null));
    }
}
