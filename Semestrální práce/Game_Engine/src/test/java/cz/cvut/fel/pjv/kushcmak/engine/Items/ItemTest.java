package cz.cvut.fel.pjv.kushcmak.engine.Items;

import static org.junit.jupiter.api.Assertions.*;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class ItemTest {

    private Item item;
    private Display display;

    @BeforeEach
    public void setUp() {
        display = new Display();
        item = new Item("Sword", ItemType.WEAPON, 10, 0, 0, 0, "A simple sword.", 100, "path/to/image.png", display);
    }

    @Test
    public void testItemAttributes() {
        assertEquals("Sword", item.getName());
        assertEquals(ItemType.WEAPON, item.getType());
        assertEquals(10, item.getAttackPoints());
        assertEquals(0, item.getArmorPoints());
        assertEquals(0, item.getHpRegen());
        assertEquals(0, item.getStaminaRecovery());
        assertEquals(100, item.getPrice());
        assertEquals("path/to/image.png", item.getImagePath());
    }

    @Test
    public void testItemPosition() {
        item.setX(50);
        item.setY(100);
        assertEquals(50, item.getX());
        assertEquals(100, item.getY());
    }
}