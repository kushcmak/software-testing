package cz.cvut.fel.pjv.kushcmak.engine.Entities;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Collider;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.World;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

public class EnemyTest {
    private Display display;
    private World world;
    private ArrayList<Collider> colliders;

    @BeforeEach
    void setUp() {
        display = new Display();
        world = display.getWorld();
        colliders = new ArrayList<>();
    }

    private Enemy createEnemy(boolean applyGravity, boolean collision) {
        Enemy enemy = new Enemy("Goblin", 100, 100, 50, 50, 10, 10, 5, 5, 50, 0, 0, 4, "A goblin", "Growl", false, true, EnemyType.GOBLIN, display);
        if (applyGravity) {
            enemy.performAction();
            enemy.performAction();
        }
        if (collision) {
            colliders.add(new Collider(75, 75, 50, 50, false));
        }
        return enemy;
    }

    @Test
    public void testCollisionAndGravity() {
        Enemy enemy = createEnemy(true, true);
        int initialX = enemy.getX();
        int initialY = enemy.getY();
        enemy.setActionTimer(60);
        enemy.performAction();
        if (Objects.equals(enemy.getDirection(), "left")) {
            assertEquals(initialX - 4, enemy.getX());
        } else {
            assertEquals(initialX + 4, enemy.getX());
        }
        assertTrue(enemy.getY() > initialY);
    }

    @Test
    public void testGravity() {
        Enemy enemy = createEnemy(true, false);
        int initialX = enemy.getX();
        int initialY = enemy.getY();
        enemy.setActionTimer(60);
        enemy.performAction();
        if (Objects.equals(enemy.getDirection(), "left")) {
            assertEquals(initialX - 4, enemy.getX());
        } else {
            assertEquals(initialX + 4, enemy.getX());
        }
        assertTrue(enemy.getY() > initialY);
    }

    @Test
    public void testCollision() {
        Enemy enemy = createEnemy(false, true);
        int initialX = enemy.getX();
        int initialY = enemy.getY();
        enemy.setActionTimer(60);
        enemy.performAction();
        if (Objects.equals(enemy.getDirection(), "left")) {
            assertEquals(initialX - 4, enemy.getX());
        } else {
            assertEquals(initialX + 4, enemy.getX());
        }
        assertEquals(initialY, enemy.getY());
    }
}