package cz.cvut.fel.pjv.kushcmak.engine.Items;

import static org.junit.jupiter.api.Assertions.*;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SlotTest {

    private Slot slot;
    private Item item;
    private Display display;

    @BeforeEach
    public void setUp() {
        display = new Display();
        item = new Item("Helmet", ItemType.HEAD_ARMOR, 0, 10, 0, 0, "A strong helmet.", 200, "path/to/helmet.png", display);
        slot = new Slot(SlotType.HEAD);
    }

    @Test
    public void testSlotAcceptItem() {
        assertTrue(slot.canAccept(item));
        assertTrue(slot.addItem(item));
        assertEquals(item, slot.getItem());
    }

    @Test
    public void testSlotRejectItem() {
        Item sword = new Item("Sword", ItemType.WEAPON, 10, 0, 0, 0, "A simple sword.", 100, "path/to/sword.png", display);
        assertFalse(slot.canAccept(sword));
        assertFalse(slot.addItem(sword));
    }

    @Test
    public void testSlotRemoveItem() {
        slot.addItem(item);
        Item removedItem = slot.removeItem();
        assertEquals(item, removedItem);
        assertNull(slot.getItem());
    }

    @Test
    public void testSlotIsOccupied() {
        assertFalse(slot.isOccupied());
        slot.addItem(item);
        assertTrue(slot.isOccupied());
    }
}