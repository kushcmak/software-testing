package cz.cvut.fel.pjv.kushcmak.engine.Entities;

import static org.junit.jupiter.api.Assertions.*;

import cz.cvut.fel.pjv.kushcmak.engine.Graphics.GameFrame;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.KeyHandler;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.MouseHandler;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayerTest {

    private Player player;
    private Display display;
    private KeyHandler keyHandler;
    private MouseHandler mouseHandler;
    private Inventory inventory;

    @BeforeEach
    public void setUp() {
        display = new Display();
        keyHandler = new KeyHandler(new GameFrame("Test Game"), display);
        mouseHandler = new MouseHandler(new GameFrame("Test Game"), display);
        inventory = new Inventory();
        player = new Player("Player", 100, 100, 50, 50, 10, 10, 5, 5, 0, 100, 100, 5, display, keyHandler, 1, 0, inventory, mouseHandler);
    }

    @Test
    public void testPlayerAttributes() {
        assertEquals("Player", player.getName());
        assertEquals(100, player.getHealth());
        assertEquals(100, player.getMaxHealth());
        assertEquals(50, player.getStamina());
        assertEquals(50, player.getMaxStamina());
        assertEquals(10, player.getAttack());
        assertEquals(10, player.getBaseAttack());
        assertEquals(5, player.getArmor());
        assertEquals(5, player.getBaseArmor());
        assertEquals(0, player.getMoney());
        assertEquals(1, player.getLevel());
        assertEquals(0, player.getExperiencePoints());
    }

    @Test
    public void testPlayerPosition() {
        player.setX(200);
        player.setY(300);
        assertEquals(200, player.getX());
        assertEquals(300, player.getY());
    }

    @Test
    public void testPlayerDie() {
        player.takeDamage(105);
        assertEquals(0, player.getHealth());
    }
}