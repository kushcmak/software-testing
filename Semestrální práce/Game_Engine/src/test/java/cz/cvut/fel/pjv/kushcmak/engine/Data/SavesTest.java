package cz.cvut.fel.pjv.kushcmak.engine.Data;

import static org.junit.jupiter.api.Assertions.*;

import cz.cvut.fel.pjv.kushcmak.engine.Entities.Player;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.Display;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.KeyHandler;
import cz.cvut.fel.pjv.kushcmak.engine.Graphics.MouseHandler;
import cz.cvut.fel.pjv.kushcmak.engine.Items.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SavesTest {

    private Saves saves;
    private Display display;
    private Player player;

    @BeforeEach
    public void setUp() {
        display = new Display();
        saves = new Saves(display);
        player = new Player("Player", 100, 100, 50, 50, 10, 10, 5, 5, 0, 100, 100, 5, display, new KeyHandler(null, display), 1, 0, new Inventory(), new MouseHandler(null, display));
        display.player = player;
    }

    @Test
    public void testSaveAndLoad() {
        player.setHealth(80);
        player.setMoney(500);
        saves.save();

        player.setHealth(100);
        player.setMoney(0);
        saves.load();

        assertEquals(80, player.getHealth());
        assertEquals(500, player.getMoney());
    }
}